﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program_for_test
{
    class Program
    {
        static void Main(string[] args)
        {



            #region Create group of list sample to apply to Resquest option by vendors

            var listCarsByVendors=  from carro in carros
                                    group carro by carro.VendorId into newGroup
             orderby newGroup.Key
              select newGroup;
            foreach (var nameGroup in listCarsByVendors)
            {
                Console.WriteLine($"Key: {nameGroup.Key}");
                foreach (var carro in nameGroup)
                {
                    Console.WriteLine($"\t{carro.Marca}, {carro.Precio}");
                }
            }


            #endregion

            #region Test conversion double to Decimal

            #region Inside
            //double precio = 5.3;
            //decimal depre = Convert.ToDecimal(10.5);
            //decimal depre2 = Convert.ToDecimal(precio);
            //Console.WriteLine("Decimal quantity value: " + depre);

            #endregion

            #endregion


            Console.ReadLine();


        }
      

        protected class Carro {

            public int VendorId { get; set; }
            public string Marca { get; set; }
            public int Precio { get; set; }
        }

        protected static List<Carro> carros = new List<Carro> {

            new Carro{ VendorId=1,Marca="Kia",Precio=152000},
            new Carro{ VendorId=2,Marca="Volskvagen",Precio=162000},
            new Carro{ VendorId=2,Marca="Ford",Precio=172000},
            new Carro{ VendorId=1,Marca="Susuki",Precio=182000},
            new Carro{ VendorId=1,Marca="Honda",Precio=192000},
            new Carro{ VendorId=3,Marca="Sentra",Precio=12000},
            new Carro{ VendorId=4,Marca="Chevrolet",Precio=162000},
            new Carro{ VendorId=4,Marca="Nissan",Precio=152000},
            new Carro{ VendorId=4,Marca="Jeep",Precio=142000},
            new Carro{ VendorId=4,Marca="Toyota",Precio=182000},
            new Carro{ VendorId=1,Marca="Yaris",Precio=192000},
            new Carro{ VendorId=2,Marca="Kia",Precio=152000},
            new Carro{ VendorId=2,Marca="Kia",Precio=162000},
            new Carro{ VendorId=2,Marca="Kia",Precio=142000},
            new Carro{ VendorId=2,Marca="Kia",Precio=112000}


        };


    }
}
