﻿//------------------------------------------------------------------------------
// Contributor(s): Crowster Gozalez Martinez 07/08/2019, Tlaxcala, Mexico
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Vendors;
using Nop.Core.Http.Extensions;
using Nop.Core.Infrastructure;
using Nop.Core.Plugins;
using Nop.Plugin.Shipping.Pakke;
using Nop.Plugin.Shipping.Pakke.Domain;
using Nop.Plugin.Shipping.Pakke.Models;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Shipping;
using Nop.Services.Shipping.Tracking;
using Nop.Services.Vendors;
using PakkeCoreLibrary.Model;
//using PakkeLibrary.Model;

namespace Nop.Plugin.Shipping.Pakke
{
    /// <summary>
    /// Pakke computation method
    /// </summary>
    public class PakkeComputationMethod : BasePlugin, IShippingRateComputationMethod
    {
        #region Constants

        private const int MAXPACKAGEWEIGHT = 60;
        private const string MEASUREWEIGHTSYSTEMKEYWORD = "kg";
        private const string MEASUREDIMENSIONSYSTEMKEYWORD = "cm";
        // private const string Base_URL = "https://seller.pakke.mx/api/v1/";
        // private const string AccessToken = "HaSfouuKwu8gpDxr63KdBIRINv9MjfnlrOlsnXYhWx5zuLF7wmbI6mkisADAwwvW";
        #endregion

        #region Fields

        private readonly IMeasureService _measureService;
        private readonly IShippingService _shippingService;
        private readonly IVendorService _vendorService;
        private readonly IAddressService _addressService;

        private readonly ISettingService _settingService;
        private readonly PakkeSettings _pakkeSettings;
        private readonly ICountryService _countryService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly ILogger _logger;
        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;

        private readonly StringBuilder _traceMessages;

        #endregion

        #region Ctor

        public PakkeComputationMethod(IMeasureService measureService,
            IShippingService shippingService,
            ISettingService settingService,
            PakkeSettings pakkeSettings,
            ICountryService countryService,
            ICurrencyService currencyService,
            CurrencySettings currencySettings,
            IOrderTotalCalculationService orderTotalCalculationService,
            ILogger logger,
            ILocalizationService localizationService,
            IVendorService vendorService,
            IAddressService addressService,
            IWebHelper webHelper)
        {
            this._measureService = measureService;
            this._shippingService = shippingService;
            this._settingService = settingService;
            this._pakkeSettings = pakkeSettings;
            this._countryService = countryService;
            this._currencyService = currencyService;
            this._currencySettings = currencySettings;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._logger = logger;
            this._localizationService = localizationService;
            this._webHelper = webHelper;
            this._vendorService = vendorService;
            this._addressService = addressService;
            this._traceMessages = new StringBuilder();
        }
        #endregion

        #region Utilities
        /// <summary>
        /// This method create the pachages by vendors and validate all the products in the cart
        /// </summary>
        /// <param name="getShippingOptionRequest">Shipping option request with all the products in the shop cart</param>
        /// <returns></returns>
        public PakkeCoreLibrary.Model.PackagePakke CreateRequest(GetShippingOptionRequest getShippingOptionRequest, ref GetShippingOptionResponse response)
        {

            PakkeCoreLibrary.Model.PackagePakke result;
            try
            {

                //This method implements the logic to create the correct packages , it usually can be one or multiple products, so if multiple products enter to the same 
                //package, the system only will to generate one package

                result = new PakkeCoreLibrary.Model.PackagePakke();

                PakkeCoreLibrary.Model.PackagePakke listPackagesFromClaculationByIndividualLineItems = SetIndividualPackageLineItems(getShippingOptionRequest);

                result.listPackages = listPackagesFromClaculationByIndividualLineItems.listPackages;

            }
            catch (Exception ex)
            {
                response.AddError(ex.Message);
                throw;
            }
            return result;


        }


        /// <summary>
        /// Create the packages in line items
        /// </summary>
        /// <param name="getShippingOptionRequest"></param>
        /// <returns></returns>
        private PakkeCoreLibrary.Model.PackagePakke SetIndividualPackageLineItems(GetShippingOptionRequest getShippingOptionRequest)
        {

            int vendorId = 0;
            PakkeCoreLibrary.Model.PackagePakke result = new PakkeCoreLibrary.Model.PackagePakke();
            List<PakkeCoreLibrary.Model.PackagePakke> listPackagePakke = new List<PakkeCoreLibrary.Model.PackagePakke>();

            //Group packages list in the cart by vendors
            var listRequestByVendors = from product in getShippingOptionRequest.Items
                                       group product by product.ShoppingCartItem.Product.VendorId into productByVendor
                                       orderby productByVendor.Key
                                       select productByVendor;
            //Read the list of products by vendors anc calculate the correct package information

            foreach (var products in listRequestByVendors)
            {
                //Is necessary to reset the items list by products in the list request vendors
                getShippingOptionRequest.Items = null;
                //Set the correct items list by vendor
                GetShippingOptionRequest getShippingOptionReq = new GetShippingOptionRequest();
                getShippingOptionReq.Items = products.ToList();
                //Product key is de vendor id
                vendorId = products.Key;
                // foreach (var product in products)
                // {                    
                // GEt the used mesaure weigth, in this case Kg
                var usedMeasureWeight = GetUsedMeasureWeight();
                // GEt the used mesaure weigth, in this case Cm

                var usedMeasureDimension = GetUsedMeasureDimension();

                //Check all the products in the cart and get the max width, height,length etc.
                _shippingService.GetDimensions(getShippingOptionReq.Items, out decimal widthTmp, out decimal lengthTmp, out decimal heightTmp, true);


                //Validate the correct dimensions by the usedMEasure configuren in TaxRate configurations
                var length = ConvertFromPrimaryMeasureDimension(lengthTmp, usedMeasureDimension);
                var height = ConvertFromPrimaryMeasureDimension(heightTmp, usedMeasureDimension);
                var width = ConvertFromPrimaryMeasureDimension(widthTmp, usedMeasureDimension);
                var weight = ConvertFromPrimaryMeasureWeight(_shippingService.GetTotalWeight(getShippingOptionRequest.Customer, getShippingOptionReq.Items, ignoreFreeShippedItems: true), usedMeasureWeight);
                if (length < 1)
                    length = 1;
                if (height < 1)
                    height = 1;
                if (width < 1)
                    width = 1;
                if (weight < 1)
                    weight = 1;
                //Validate if the package is too weight and Too large(check if always is necessary two validations at the same time)
                if ((!IsPackageTooHeavy(weight)) && (!IsPackageTooLarge(length, height, width)))
                {
                    //if (!_pakkeSettings.PassDimensions)
                     //   length = width = height = 0;

                    //This code is commented if is necessary in the future to add insuarence for the product

                    //var insuranceAmount = _pakkeSettings.InsurePackage ? Convert.ToInt32(orderSubTotal) : 0;

                    PakkeCoreLibrary.Model.PackagePakke package = new PakkeCoreLibrary.Model.PackagePakke();

                    //Get vendor information
                    Vendor vendor = _vendorService.GetVendorById(vendorId);
                    //Get vendor address by address id
                    Address address = _addressService.GetAddressById(vendor.AddressId);
                    PakkeCoreLibrary.Model.Rate rate2 = new PakkeCoreLibrary.Model.Rate();

                    //Set the postal code information from origen and destiny
                    rate2.ZipCodeTo = getShippingOptionRequest.ShippingAddress.ZipPostalCode;
                    rate2.ZipCodeFrom = address.ZipPostalCode;
                    package.rate = rate2;
                    //Set package vendor information
                    package.vendor = vendor;
                    //Set package customer information

                    package.customer = getShippingOptionRequest.Customer;

                    package.Description = "Uno o varios productos en un solo paquete ";
                    PakkeCoreLibrary.Model.ParcelEntity parcel = new PakkeCoreLibrary.Model.ParcelEntity();
                    package.noPackage = 1;
                    parcel.Height = height;
                    parcel.Width = width;
                    parcel.Length = length;
                    parcel.Weight = weight;
                    package.parcel = parcel;
                    listPackagePakke.Add(package);


                }
                else
                //If the weight and large are big, we need to implement a logic to calculate the package dimensions
                {
                    var totalPackagesDims = 1;
                    var totalPackagesWeights = 1;
                    if (IsPackageTooHeavy(weight))
                    {
                        totalPackagesWeights = Convert.ToInt32(Math.Ceiling((decimal)weight / (decimal)MAXPACKAGEWEIGHT));
                    }
                    if (IsPackageTooLarge(length, height, width))
                    {
                        totalPackagesDims = Convert.ToInt32(Math.Ceiling((decimal)TotalPackageSize(length, height, width) / (decimal)108));
                    }
                    string packageUsed = string.Empty;
                    //Evaluateh the packages by dimensions and by weight and take the major.
                    var totalPackages = totalPackagesDims > totalPackagesWeights ? totalPackagesDims : totalPackagesWeights;
                    packageUsed = totalPackagesDims > totalPackagesWeights ? "Paquete por Dimensión." : "Paquete por peso";

                    if (totalPackages == 0)
                        totalPackages = 1;

                    var weight2 = weight / totalPackages;
                    var height2 = height / totalPackages;
                    var width2 = width / totalPackages;
                    var length2 = length / totalPackages;
                    if (weight2 < 1)
                        weight2 = 1;
                    if (height2 < 1)
                        height2 = 1;
                    if (width2 < 1)
                        width2 = 1;
                    if (length2 < 1)
                        length2 = 1;

                    //if (!_pakkeSettings.PassDimensions)
                     //   length2 = width2 = height2 = 0;

                    //The maximum declared amount per package: ?. thi smethod is commented for future requirement
                    // var insuranceAmountPerPackage = _pakkeSettings.InsurePackage ? Convert.ToInt32(orderSubTotal / totalPackages) : 0;
                    result.totalPackages = totalPackages;
                    for (var i = 0; i < totalPackages; i++)
                    {
                        //Instance of the package 
                        PakkeCoreLibrary.Model.PackagePakke package = new PakkeCoreLibrary.Model.PackagePakke();
                        try
                        {
                            //Get vednor information
                            Vendor vendor = _vendorService.GetVendorById(vendorId);
                            //Get vendor address by address id
                            Address address = _addressService.GetAddressById(vendor.AddressId);
                            PakkeCoreLibrary.Model.Rate rate = new PakkeCoreLibrary.Model.Rate();

                            //Validate the correct information
                            rate.ZipCodeTo = getShippingOptionRequest.ShippingAddress.ZipPostalCode;
                            rate.ZipCodeFrom = address.ZipPostalCode;
                            package.rate = rate;
                            //Set package vendor information
                            package.vendor = vendor;
                            //Set package customer information

                            package.customer = getShippingOptionRequest.Customer;

                            package.Description = packageUsed + "(Paquetes Totales x Dimension:" + totalPackagesDims + ",Paquetes Totales x Peso: " + totalPackagesWeights + ")";
                            PakkeCoreLibrary.Model.ParcelEntity parcel = new PakkeCoreLibrary.Model.ParcelEntity();
                            package.noPackage = i + 1;
                            parcel.Height = height2;
                            parcel.Width = width2;
                            parcel.Length = length2;
                            parcel.Weight = weight2;
                            package.parcel = parcel;
                            listPackagePakke.Add(package);
                        }
                        catch (Exception ex)
                        {

                            throw new Exception("Error en creación paquetes: " + ex);
                        }
                    }
                }

                // }
                result.listPackages = listPackagePakke;
                //throw new Exception("asd");
            }
            return result;
        }

        #region Option method to calculate packages dimension
        #region inside
        /*
               private PackagePakke SetIndividualPackageLineItemsOneItemPerPackage( GetShippingOptionRequest getShippingOptionRequest )
        {
            //Set the container for the result
            PackagePakke result=new PackagePakke();

            // Rate request setup - each Shopping Cart Item is a separate package

            var usedMeasureWeight = GetUsedMeasureWeight();
            var usedMeasureDimension = GetUsedMeasureDimension();

            foreach (var packageItem in getShippingOptionRequest.Items)
            {
                var sci = packageItem.ShoppingCartItem;
                var qty = packageItem.GetQuantity();

                //get dimensions for qty 1
                _shippingService.GetDimensions(new List<GetShippingOptionRequest.PackageItem>
                                               {
                                                   new GetShippingOptionRequest.PackageItem(sci, 1)
                                               }, out decimal widthTmp, out decimal lengthTmp, out decimal heightTmp, true);

                var length = ConvertFromPrimaryMeasureDimension(lengthTmp, usedMeasureDimension);
                var height = ConvertFromPrimaryMeasureDimension(heightTmp, usedMeasureDimension);
                var width = ConvertFromPrimaryMeasureDimension(widthTmp, usedMeasureDimension);
                var weight = ConvertFromPrimaryMeasureWeight(sci.Product.Weight, usedMeasureWeight);
                if (length < 1)
                    length = 1;
                if (height < 1)
                    height = 1;
                if (width < 1)
                    width = 1;
                if (weight < 1)
                    weight = 1;

                //The maximum declared amount per package: 50000 USD.
                //TODO: Currently using Product.Price - should we use GetUnitPrice() instead?
                // Convert.ToInt32(_priceCalculationService.GetUnitPrice(sci, includeDiscounts:false))
                //One could argue that the insured value should be based on Cost rather than Price.
                //GetUnitPrice handles Attribute Adjustments and also Customer Entered Price.
                //But, even with includeDiscounts:false, it could apply a "discount" from Tier pricing.
                
                //var insuranceAmountPerPackage = _pakkeSettings.InsurePackage ? Convert.ToInt32(sci.Product.Price) : 0;
              
                result.totalPackages = qty;
                List<PackagePakke> listPackages = new List<PackagePakke>();

                for (var j = 0; j < qty; j++)
                {
                    PackagePakke package = new PackagePakke();
                    package.noPackage = j + 1;

                    ParcelEntity parcel = new ParcelEntity();
                    parcel.Height = height;
                    parcel.Width = width;
                    parcel.Length = length;
                    parcel.Weight = weight;
                    package.parcel = parcel;
                    listPackages.Add(package);
                    // AppendPackageRequest(sb, packagingType, length, height, width, weight, insuranceAmountPerPackage, currencyCode);
                }
                result.listPackages = listPackages;
            }
            return result;

        }

        private PackagePakke  SetIndividualPackageLineItemsCubicRootDimensions(  GetShippingOptionRequest getShippingOptionRequest )
        {

            //Set the container for the result
            PackagePakke result;

            // Rate request setup - Total Volume of Shopping Cart Items determines number of packages

            //Dimensional weight is based on volume (the amount of space a package
            //occupies in relation to its actual weight). If the cubic size of your
            //package measures three cubic feet (5,184 cubic inches or 84,951
            //cubic centimetres) or greater, you will be charged the greater of the
            //dimensional weight or the actual weight.
            //This algorithm devides total package volume by the UPS settings PackingPackageVolume
            //so that no package requires dimensional weight; this could result in an under-charge.

            var usedMeasureWeight = GetUsedMeasureWeight();
            var usedMeasureDimension = GetUsedMeasureDimension();

            int totalPackagesDims;
            int length;
            int height;
            int width;

            if (getShippingOptionRequest.Items.Count == 1 && getShippingOptionRequest.Items[0].GetQuantity() == 1)
            {
                var sci = getShippingOptionRequest.Items[0].ShoppingCartItem;

                //get dimensions for qty 1
                _shippingService.GetDimensions(new List<GetShippingOptionRequest.PackageItem>
                                               {
                                                   new GetShippingOptionRequest.PackageItem(sci, 1)
                                               }, out decimal widthTmp, out decimal lengthTmp, out decimal heightTmp, true);

                totalPackagesDims = 1;
                length = ConvertFromPrimaryMeasureDimension(lengthTmp, usedMeasureDimension);
                height = ConvertFromPrimaryMeasureDimension(heightTmp, usedMeasureDimension);
                width = ConvertFromPrimaryMeasureDimension(widthTmp, usedMeasureDimension);
            }
            else
            {
                decimal totalVolume = 0;
                foreach (var item in getShippingOptionRequest.Items)
                {
                    var sci = item.ShoppingCartItem;

                    //get dimensions for qty 1
                    _shippingService.GetDimensions(new List<GetShippingOptionRequest.PackageItem>
                                               {
                                                   new GetShippingOptionRequest.PackageItem(sci, 1)
                                               }, out decimal widthTmp, out decimal lengthTmp, out decimal _, true);

                    var productLength = ConvertFromPrimaryMeasureDimension(lengthTmp, usedMeasureDimension);
                    var productHeight = ConvertFromPrimaryMeasureDimension(lengthTmp, usedMeasureDimension);
                    var productWidth = ConvertFromPrimaryMeasureDimension(widthTmp, usedMeasureDimension);
                    totalVolume += item.GetQuantity() * (productHeight * productWidth * productLength);
                }

                int dimension;
                if (totalVolume == 0)
                {
                    dimension = 0;
                    totalPackagesDims = 1;
                }
                else
                {
                    // cubic inches
                    var packageVolume = _pakkeSettings.PackingPackageVolume;
                    if (packageVolume <= 0)
                        packageVolume = 60;//5184;

                    // cube root (floor)
                    dimension = Convert.ToInt32(Math.Floor(Math.Pow(Convert.ToDouble(packageVolume), (double)(1.0 / 3.0))));
                    if (IsPackageTooLarge(dimension, dimension, dimension))
                        throw new NopException("pakkeSettings.PackingPackageVolume exceeds max package size");

                    // adjust packageVolume for dimensions calculated
                    packageVolume = dimension * dimension * dimension;

                    totalPackagesDims = Convert.ToInt32(Math.Ceiling(totalVolume / packageVolume));
                }

                length = width = height = dimension;
            }
            if (length < 1)
                length = 1;
            if (height < 1)
                height = 1;
            if (width < 1)
                width = 1;

            var weight = ConvertFromPrimaryMeasureWeight(_shippingService.GetTotalWeight(getShippingOptionRequest.Customer,getShippingOptionRequest.Items, ignoreFreeShippedItems: true), usedMeasureWeight);
            if (weight < 1)
                weight = 1;

            var totalPackagesWeights = 1;
            if (IsPackageTooHeavy(weight))
            {
                totalPackagesWeights = Convert.ToInt32(Math.Ceiling((decimal)weight / (decimal)MAXPACKAGEWEIGHT));
            }

            var totalPackages = totalPackagesDims > totalPackagesWeights ? totalPackagesDims : totalPackagesWeights;

            var weightPerPackage = weight / totalPackages;

            //The maximum declared amount per package: 50000 USD.
            //var insuranceAmountPerPackage = _pakkeSettings.InsurePackage ? Convert.ToInt32(orderSubTotal / totalPackages) : 0;

       
            result = new PackagePakke();
            result.totalPackages = totalPackages;

            List<PackagePakke> listPackages = new List<PackagePakke>();

            for (var i = 0; i < totalPackages; i++)
            {
                PackagePakke package = new PackagePakke();
                package.noPackage = i + 1;

                ParcelEntity parcel = new ParcelEntity();
                parcel.Height = height;
                parcel.Width = width;
                parcel.Length = length;
                parcel.Weight = weight;
                package.parcel = parcel;
                listPackages.Add(package);

                // AppendPackageRequest(sb, packagingType, length, height, width, weightPerPackage, insuranceAmountPerPackage, currencyCode);
            }
            result.listPackages = listPackages;
            return result;
        }

             */
        #endregion
        #endregion

        /// <summary>
        /// VAlidate if the pachage is too large
        /// </summary>
        /// <param name="length"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        private bool IsPackageTooLarge(int length, int height, int width)
        {
            var total = TotalPackageSize(length, height, width);
            //if total > Max grind  return true else false
            //This value is the standard in the couriers service
            return total > (419); // +270);// 165;
        }
        /// <summary>
        /// Calculate total package size
        /// </summary>
        /// <param name="length"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        private int TotalPackageSize(int length, int height, int width)
        {

            //calcular la circunferencia
            var girth = height + height + width + width;
            var total = girth + length;
            return total;
        }
        /// <summary>
        /// VAlidate if the package is to weight
        /// </summary>
        /// <param name="weight"></param>
        /// <returns></returns>
        private bool IsPackageTooHeavy(int weight)
        {
            return weight > MAXPACKAGEWEIGHT;
        }
         
        private MeasureWeight GetUsedMeasureWeight()
        {
            var usedMeasureWeight = _measureService.GetMeasureWeightBySystemKeyword(MEASUREWEIGHTSYSTEMKEYWORD);
            if (usedMeasureWeight == null)
                throw new NopException("Pakke shipping service. Could not load \"{0}\" measure weight", MEASUREWEIGHTSYSTEMKEYWORD);
            return usedMeasureWeight;
        }

        private MeasureDimension GetUsedMeasureDimension()
        {
            var usedMeasureDimension = _measureService.GetMeasureDimensionBySystemKeyword(MEASUREDIMENSIONSYSTEMKEYWORD);
            if (usedMeasureDimension == null)
                throw new NopException("Pakke shipping service. Could not load \"{0}\" measure dimension", MEASUREDIMENSIONSYSTEMKEYWORD);

            return usedMeasureDimension;
        }

        private int ConvertFromPrimaryMeasureDimension(decimal quantity, MeasureDimension usedMeasureDimension)
        {
            return Convert.ToInt32(Math.Ceiling(_measureService.ConvertFromPrimaryMeasureDimension(quantity, usedMeasureDimension)));
        }
        private int ConvertFromPrimaryMeasureWeight(decimal quantity, MeasureWeight usedMeasureWeighht)
        {
            return Convert.ToInt32(Math.Ceiling(_measureService.ConvertFromPrimaryMeasureWeight(quantity, usedMeasureWeighht)));
        }
        private IEnumerable<ShippingOption> ParseResponse2(PakkeCoreLibrary.Model.PakkeEntity response, ref string error)
        {
            var shippingOptions = new List<ShippingOption>();

            //var carrierServicesOffered = _pakkeSettings.CarrierServicesOffered;

            foreach (var item in response.Pakke)
            {

                //Weed out unwanted or unknown service rates
                // if (service.ToUpper() != "UNKNOWN")
                //{
                if (item.BestOption)
                {
                    var shippingOption = new ShippingOption
                    {
                        Rate = Convert.ToDecimal(item.TotalPrice, new CultureInfo("es-ES")),
                        Name = item.CourierName
                    };
                    shippingOptions.Add(shippingOption);
                    // }
                }
            }
            return shippingOptions;
        }
        #endregion
        #region Methods
        /// <summary>
        ///  Gets available shipping options
        /// </summary>
        /// <param name="getShippingOptionRequest">A request for getting shipping options</param>
        /// <returns>Represents a response of getting shipping rate options</returns>
        public GetShippingOptionResponse GetShippingOptions(GetShippingOptionRequest getShippingOptionRequest)
        {

            if (getShippingOptionRequest == null)
                throw new ArgumentNullException(nameof(getShippingOptionRequest));

            var response = new GetShippingOptionResponse();


            if (getShippingOptionRequest.Items == null)
            {
                response.AddError("No shipment items");
                return response;
            }

            if (getShippingOptionRequest.ShippingAddress == null)
            {
                response.AddError("Shipping address is not set");
                return response;
            }

            if (getShippingOptionRequest.ShippingAddress.Country == null)
            {
                response.AddError("Shipping country is not set");
                return response;
            }

            if (getShippingOptionRequest.CountryFrom == null)
            {
                getShippingOptionRequest.CountryFrom = _countryService.GetAllCountries().FirstOrDefault();
            }
            try
            {
                PakkeCoreLibrary.Model.PackagePakke packages = CreateRequest(getShippingOptionRequest, ref response);
                double TotalTaxRate = 0;
                TotalTaxRate = CalculateTotalTaxRate(packages);
                //This method will be after payment, and the result will be read

                ISession session = EngineContext.Current.Resolve<IHttpContextAccessor>().HttpContext.Session;

                //  Nop.Core.Infrastructure.EngineContext.Current.Resolve<IHttpContextAccessor>().HttpContext.Session.GetString("TaxRate");
                // Nop.Core.Infrastructure.EngineContext.Current.Resolve<IHttpContextAccessor>().HttpContext.cu

                List<PakkeCoreLibrary.Model.ShipmentRequest> shipmentRequestList = ShipmentRequestList(packages, getShippingOptionRequest.ShippingAddress.Id, ref response);
                session.Set("PackagesInformation", shipmentRequestList);
                session.Set("PakkeBaseURL", _pakkeSettings.Url);
                session.Set("PakkeToken", _pakkeSettings.AccessKey);

                //PakkeCoreLibrary.Services.CouriersService.GenerateReference()

                var error = "";
                if (string.IsNullOrEmpty(error))
                {
                    ShippingOption shippingOption = new ShippingOption();
                    shippingOption.Name = $"(" + packages.listPackages.Count.ToString() + ")Costo de envío total ";
                    shippingOption.Rate = Convert.ToDecimal(TotalTaxRate);    //+= _pakkeSettings.AdditionalHandlingCharge, code commented if is necessary to add more taxes;
                    response.ShippingOptions.Add(shippingOption);
                }
                else
                {
                    response.AddError(error);
                }

                if (response.ShippingOptions.Any())
                    response.Errors.Clear();
            }
            catch (Exception exc  )
            {
                response.AddError($"Servicio de paqueteria no disponible, intente nuevamente mas tarde. ");
               
                //response.AddError($"Cotizador Pakke no disponible, intente mas tarde. ");
                _logger.Information("Servicio de paqueteria no disponible, intente nuevamente mas tarde.", new Exception(exc.ToString()), getShippingOptionRequest.Customer);

            }
            finally
            {
                if (_pakkeSettings.Tracing && _traceMessages.Length > 0)
                {
                    var shortMessage =
                        $"Pakke obiene opciones de envio del cliente {getShippingOptionRequest.Customer.Email}.  {getShippingOptionRequest.Items.Count} objetos en el carrito";
                    _logger.Information(shortMessage, new Exception(_traceMessages.ToString()), getShippingOptionRequest.Customer);
                }
            }

            return response;
        }
        /// <summary>
        /// GEt the total tax rate, readin gall the packages ifnormation
        /// </summary>
        /// <param name="package">Package information</param>
        /// <returns></returns>
        public double CalculateTotalTaxRate(PakkeCoreLibrary.Model.PackagePakke package)
        {
            double totalTaxRate = 0;
            string message = string.Empty;
            foreach (var item in package.listPackages)
            {
                //Set the package data information (parcel)
                PakkeCoreLibrary.Model.ParcelEntity parcel = new PakkeCoreLibrary.Model.ParcelEntity();
                parcel.Height = item.parcel.Height; //altura     
                parcel.Width = item.parcel.Width;//ancho
                parcel.Length = item.parcel.Length;//longitud
                parcel.Weight = item.parcel.Weight;//peso
                PakkeCoreLibrary.Model.Rate rate = new PakkeCoreLibrary.Model.Rate();
                rate.parcel = parcel;
                rate.ZipCodeFrom = item.rate.ZipCodeFrom;//zipCodeFrom; //Codigo postal del vendedor, o proovedor
                rate.ZipCodeTo = item.rate.ZipCodeTo;// getShippingOptionRequest.ZipPostalCodeFrom; //Codigo postal del cliente

                //Calculate the tax rate per product
                PakkeCoreLibrary.Model.PakkeEntity result = PakkeCoreLibrary.Services.CouriersService.GetRates(_pakkeSettings.Url, rate, _pakkeSettings.AccessKey, _pakkeSettings.Tracing);
                //Get best courier service
                PakkeCoreLibrary.Model.PakkeEntity pakkeEntity = GetBestTaxRate(result);
                totalTaxRate = totalTaxRate + pakkeEntity.TotalPrice;
                message += "\n \n Product information: package No." + item.noPackage + ",Largo: " + item.parcel.Length +
                    ", Ancho: " + item.parcel.Width + ", Altura: " + item.parcel.Height + ", Peso: " +
                    item.parcel.Weight + " ,Código Postal origen: " + item.rate.ZipCodeFrom + ", Código Postal destino: " +
                    item.rate.ZipCodeTo + " \n Vendedor: " + item.vendor.Name + ",Costo de envio:" + pakkeEntity.TotalPrice +
                    " ,Total Price:" + totalTaxRate + " Calculado por: " + item.Description;
            }
            if (_pakkeSettings.Tracing && message.Length > 0)
            {
                _logger.Information("TraceLog- CalculateTotalTaxRate", new Exception("Informacion: \n" + message));
            }
            return totalTaxRate;
        }
        /// <summary>
        ///  Complete the list of packages with courier service information
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public List<PakkeCoreLibrary.Model.PackagePakkeEntity> CompletePackageInformationForReferenceRate(PakkeCoreLibrary.Model.PackagePakke package)
        {
            //PakkeCoreLibrary.Model.PackagePakke result = new PakkeCoreLibrary.Model.PackagePakke() ;
            List<PakkeCoreLibrary.Model.PackagePakkeEntity> listPackaesFullInformation = new List<PakkeCoreLibrary.Model.PackagePakkeEntity>();

            double totalTaxRate = 0;
            string message = string.Empty;
            foreach (var item in package.listPackages)
            {

                //Set the package data information (parcel)
                PakkeCoreLibrary.Model.ParcelEntity parcel = new PakkeCoreLibrary.Model.ParcelEntity();
                parcel.Height = item.parcel.Height; //altura     
                parcel.Width = item.parcel.Width;//ancho
                parcel.Length = item.parcel.Length;//longitud
                parcel.Weight = item.parcel.Weight;//peso
                PakkeCoreLibrary.Model.Rate rate = new PakkeCoreLibrary.Model.Rate();
                rate.parcel = parcel;
                rate.ZipCodeFrom = item.rate.ZipCodeFrom;//zipCodeFrom; //Codigo postal del vendedor, o proovedor
                rate.ZipCodeTo = item.rate.ZipCodeTo;// getShippingOptionRequest.ZipPostalCodeFrom; //Codigo postal del cliente

                //Calculate the tax rate per product
                PakkeCoreLibrary.Model.PakkeEntity resultRates = PakkeCoreLibrary.Services.CouriersService.GetRates(_pakkeSettings.Url, rate, _pakkeSettings.AccessKey, _pakkeSettings.Tracing);

                //Get best courier service
                PakkeCoreLibrary.Model.PakkeEntity pakkeEntity = GetBestTaxRate(resultRates);

                totalTaxRate = totalTaxRate + pakkeEntity.TotalPrice;

                //Add additional information to the package to generate the reference

                PakkeCoreLibrary.Model.PackagePakkeEntity packageEntity = new PakkeCoreLibrary.Model.PackagePakkeEntity();
                packageEntity.pakkeEntity = pakkeEntity;
                item.pakkeEntity = pakkeEntity;

                packageEntity.noPackage = item.noPackage;
                packageEntity.parcel = item.parcel;
                packageEntity.rate = item.rate;
                packageEntity.totalPackages = item.totalPackages;
                packageEntity.Description = item.Description;

                listPackaesFullInformation.Add(packageEntity);
                message += "\n \n Product information: package No." + item.noPackage + ",Largo: " + item.parcel.Length + ", Ancho: " + item.parcel.Width + ", Altura: " +
                    item.parcel.Height + ", Peso: " + item.parcel.Weight + " ,Código Postal origen: " +
                    item.rate.ZipCodeFrom + ", Código Postal destino: " + item.rate.ZipCodeTo + " \n Vendedor: " + item.vendor.Name + ",Costo de envio:" +
                    pakkeEntity.TotalPrice + " ,Total Price:" + totalTaxRate + " Calculado por: " + item.Description + "\n , Codigo de Mensajeria:" + pakkeEntity.CourierCode +
                    ", Nombre mensajeria :" + pakkeEntity.CourierName + ", ID mensajeria: " + pakkeEntity.CourierServiceId + ", Dia estimado de entrega: " + pakkeEntity.EstimatedDeliveryDate +
                    ", Nombre servicio de Mensajeria: " + pakkeEntity.CourierServiceName + ", Dias de entrega: " + pakkeEntity.DeliveryDays;
            }

            if (_pakkeSettings.Tracing && message.Length > 0)
            {
                _logger.Information("TraceLog-Generate Rate Reference", new Exception("Informacion: \n" + message));

            }
            //result.listPackages = listPackaesFullInformation;

            return listPackaesFullInformation;
        }
        /// <summary>
        /// Create tge lis fo shipingRequest
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public List<PakkeCoreLibrary.Model.ShipmentRequest> ShipmentRequestList(PakkeCoreLibrary.Model.PackagePakke package, int customerAddressId, ref GetShippingOptionResponse response)
        {
            //PakkeCoreLibrary.Model.PackagePakke result = new PakkeCoreLibrary.Model.PackagePakke() ;
            List<PakkeCoreLibrary.Model.ShipmentRequest> shipmentRequestList = new List<PakkeCoreLibrary.Model.ShipmentRequest>();
            double totalTaxRate = 0;
            string message = string.Empty;
            _logger.Information("Information,Enter ShipmentRequestList", new Exception("Informacion: \n" ));

            foreach (var item in package.listPackages)
            {
                //Instance of shipmentRequest
                ShipmentRequest shipmentRequest = new ShipmentRequest();
                Nop.Core.Domain.Common.Address addressVendor = _addressService.GetAddressById(item.vendor.AddressId);
                PakkeCoreLibrary.Model.AddressFromEntity addressFrom=null;
                try
                {
                    addressFrom = new PakkeCoreLibrary.Model.AddressFromEntity()
                    {
                        Address1 = !string.IsNullOrEmpty(addressVendor.Address1) ? addressVendor.Address1 : " ",
                        Address2 = !string.IsNullOrEmpty(addressVendor.Address1) ? addressVendor.Address1 : addressVendor.Address1,
                        City = !string.IsNullOrEmpty(addressVendor.City) ? addressVendor.City : " ",
                        Neighborhood = !string.IsNullOrEmpty(addressVendor.City) ? addressVendor.City : " ",
                        Residential = false,
                        State = "MX-" + (!string.IsNullOrEmpty(addressVendor.StateProvince.Abbreviation) ? addressVendor.StateProvince.Abbreviation : addressVendor.County.Substring(0, 3)),
                        ZipCode = !string.IsNullOrEmpty(addressVendor.ZipPostalCode) ? addressVendor.ZipPostalCode : " ",
                    };
                }
                catch (Exception ex)
                {
                    response.AddError("Error, Fallo al obtener direccion del vendedor");
                    _logger.Error("Error, Fallo al obtener direccion del vendedor", ex);
                   // throw new Exception("Error, Fallo al obtener direccion del vendedor ");
                }
                _logger.Information("Information,Enter ShipmentRequestList", new Exception("Informacion2: \n"));
                Nop.Core.Domain.Common.Address addressCustomer = _addressService.GetAddressById(customerAddressId);
                PakkeCoreLibrary.Model.AddressToEntity addressTo=null;
                //Set Address To
                try
                {
                    addressTo = new PakkeCoreLibrary.Model.AddressToEntity()
                    {
                        Address1 = !string.IsNullOrEmpty(addressCustomer.Address1) ? addressCustomer.Address1 : " ",
                        Address2 = !string.IsNullOrEmpty(addressCustomer.Address1) ? addressCustomer.Address1 : addressCustomer.Address1,
                        City = !string.IsNullOrEmpty(addressCustomer.City) ? addressCustomer.City : " ",
                        Neighborhood = !string.IsNullOrEmpty(addressCustomer.Address2) ? addressCustomer.Address2 : " ",
                        Residential = false,
                        State = "MX-" + (!string.IsNullOrEmpty(addressCustomer.StateProvince.Abbreviation) ? addressCustomer.StateProvince.Abbreviation : addressCustomer.County.Substring(0, 3)),
                        ZipCode = !string.IsNullOrEmpty(addressCustomer.ZipPostalCode) ? addressCustomer.ZipPostalCode : " ",
                    };
                }
                catch (Exception ex)
                {
                    response.AddError("Error, Fallo al obtener direccion del cliente");
                    _logger.Error("Error, Fallo al obtener direccion del cliente", ex);
                    //throw new Exception("Error, Fallo al obtener direccion del cliente ");
                }
                _logger.Information("Information,Enter ShipmentRequestList", new Exception("Informacion3: \n"));
                //Set recipient information
                PakkeCoreLibrary.Model.RecipientEntity recipientEntity = new PakkeCoreLibrary.Model.RecipientEntity()
                {
                    CompanyName = !string.IsNullOrEmpty(addressCustomer.Company) ? addressCustomer.Company : " ",
                    Email = !string.IsNullOrEmpty(addressCustomer.Email) ? addressCustomer.Email : " ",
                    Phone1 = addressCustomer.PhoneNumber,
                    Name = addressCustomer.FirstName + " " + addressCustomer.LastName
                };
                //Set the sender information
                PakkeCoreLibrary.Model.SenderEntity senderEntity = new PakkeCoreLibrary.Model.SenderEntity()
                {
                    Email = !string.IsNullOrEmpty(item.vendor.Email)? item.vendor.Email:" ",
                    Name = !string.IsNullOrEmpty(item.vendor.Name) ? item.vendor.Name : " " ,
                    Phone1 = !string.IsNullOrEmpty(addressVendor.PhoneNumber) ? addressVendor.PhoneNumber : " ",
                    Phone2 = !string.IsNullOrEmpty(addressVendor.PhoneNumber) ? addressVendor.PhoneNumber : " ",
                };

                shipmentRequest.AddressFrom = addressFrom;
                shipmentRequest.AddressTo = addressTo;
                shipmentRequest.Parcel = item.parcel;
                shipmentRequest.Recipient = recipientEntity;
                shipmentRequest.Sender = senderEntity;

                _logger.Information("Information,Enter ShipmentRequestList", new Exception("Informacion4: \n"));
                //this reseller reference is free text to identiyfy the reference
                shipmentRequest.ResellerReference = "LegadoMexico";

                //Set the package data information (parcel)
                PakkeCoreLibrary.Model.ParcelEntity parcel = new PakkeCoreLibrary.Model.ParcelEntity();
                parcel.Height = item.parcel.Height; //altura     
                parcel.Width = item.parcel.Width;//ancho
                parcel.Length = item.parcel.Length;//longitud
                parcel.Weight = item.parcel.Weight;//peso
                PakkeCoreLibrary.Model.Rate rate = new PakkeCoreLibrary.Model.Rate();
                rate.parcel = parcel;
                rate.ZipCodeFrom = item.rate.ZipCodeFrom;//zipCodeFrom; //Codigo postal del vendedor, o proovedor
                rate.ZipCodeTo = item.rate.ZipCodeTo;// getShippingOptionRequest.ZipPostalCodeFrom; //Codigo postal del cliente

                string rateS = JsonConvert.SerializeObject(rate);
                _logger.Information("Information,resultRaShipmentRequestListtes", new Exception("Informacion: \n" + rateS));
                PakkeCoreLibrary.Model.PakkeEntity resultRates = PakkeCoreLibrary.Services.CouriersService.GetRates(_pakkeSettings.Url, rate, _pakkeSettings.AccessKey, _pakkeSettings.Tracing);
                string resultRatesString = JsonConvert.SerializeObject(resultRates);
                _logger.Information("Information,resultRates", new Exception("Informacion: \n" + resultRatesString));

                //Get best courier service
                PakkeCoreLibrary.Model.PakkeEntity pakkeEntity = GetBestTaxRate(resultRates);

                totalTaxRate = totalTaxRate + pakkeEntity.TotalPrice;
                shipmentRequest.Content = "Producto";// item.vendor.Name;
                shipmentRequest.CourierCode = pakkeEntity.CourierCode;
                shipmentRequest.CourierServiceId = pakkeEntity.CourierServiceId;

                message += "\n \n Product information: package No." + item.noPackage + ",Largo: " + item.parcel.Length + ", Ancho: " + item.parcel.Width + ", Altura: " +
                   item.parcel.Height + ", Peso: " + item.parcel.Weight + " ,Código Postal origen: " +
                   item.rate.ZipCodeFrom + ", Código Postal destino: " + item.rate.ZipCodeTo + " \n Vendedor: " + item.vendor.Name + ",Costo de envio:" +
                   pakkeEntity.TotalPrice + " ,Total Price:" + totalTaxRate + " Calculado por: " + item.Description + "\n , Codigo de Mensajeria:" + pakkeEntity.CourierCode +
                   ", Nombre mensajeria :" + pakkeEntity.CourierName + ", ID mensajeria: " + pakkeEntity.CourierServiceId + ", Dia estimado de entrega: " + pakkeEntity.EstimatedDeliveryDate +
                   ", Nombre servicio de Mensajeria: " + pakkeEntity.CourierServiceName + ", Dias de entrega: " + pakkeEntity.DeliveryDays;

                shipmentRequestList.Add(shipmentRequest);

                if (_pakkeSettings.Tracing && message.Length > 0)
                {
                    _logger.Information("TraceLog- Generate List Of SHipping Request", new Exception("Informacion: \n" + message));
                }

            } //End for
            return shipmentRequestList;
        }

        /// <summary>
        /// Validate if the products information is complete, to create a correct shipping reference, as de weight,length, width and height
        /// </summary>
        /// <returns>Returns if the validation is correct and a list of vendors</returns>
        public PakkeCoreLibrary.Model.PackagePakke CreatePackages(ref GetShippingOptionResponse response, GetShippingOptionRequest shippingrequest)
        {
            PakkeCoreLibrary.Model.PackagePakke packages = null;
            try
            {
                //Buscar en la lista de productos los mismos vendedores

                //   var totalVendors = shippingrequest.Items.GroupBy(x => x.ShoppingCartItem.Product.VendorId ).Select(g => new { Value = g.Key, Count = g.Count() }).OrderByDescending(x => x.Count);
                var GroupByVendors = from s in shippingrequest.Items group s by s.ShoppingCartItem.Product.VendorId into sg select new { sg.Key, sg };

            }
            catch (Exception)
            {
                response.AddError("Información de productos insuficientes");
                // throw new Exception("Información de productos insuficientes");
            }
            return packages;
        }
        /// <summary>
        /// GEt the best courier service option
        /// </summary>
        /// <param name="pakkeEntity"></param>
        /// <returns></returns>
        public PakkeCoreLibrary.Model.PakkeEntity GetBestTaxRate(PakkeCoreLibrary.Model.PakkeEntity pakkeEntity)
        {
            PakkeCoreLibrary.Model.PakkeEntity result = new PakkeCoreLibrary.Model.PakkeEntity();
            try
            {
                result = (from i in pakkeEntity.Pakke
                          let MinPrice= pakkeEntity.Pakke.Min(m => m.TotalPrice)
                                where i.TotalPrice == MinPrice
                          select i).FirstOrDefault();

               /* foreach (var item in pakkeEntity.Pakke)
                {

                    if (item.BestOption)
                        result = item;
                }*/
            }
            catch (Exception)
            {
                result = new PakkeCoreLibrary.Model.PakkeEntity();
            }
            return result;
        }
        /// <summary>
        /// Gets fixed shipping rate (if shipping rate computation method allows it and the rate can be calculated before checkout).
        /// </summary>
        /// <param name="getShippingOptionRequest">A request for getting shipping options</param>
        /// <returns>Fixed shipping rate; or null in case there's no fixed shipping rate</returns>
        public decimal? GetFixedRate(GetShippingOptionRequest getShippingOptionRequest)
        {
            return null;
        }

        /// <summary>
        /// Gets a configuration page URL
        /// </summary>
        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/ShippingPakke/Configure";
        }

        /// <summary>
        /// Install plugin
        /// </summary>
        public override void Install()
        {
            //settings
            var settings = new PakkeSettings
            {
                //Url = "https://www.ups.com/ups.app/xml/Rate",
                //CustomerClassification = PakkeCustomerClassification.Retail,
                //PickupType = PakkePickupType.OneTimePickup,
                //PackagingType = PakkePackagingType.ExpressBox,
               // PackingPackageVolume = 5184,
                //PackingType = PackingType.PackByDimensions,
                //PassDimensions = true,
            };
            _settingService.SaveSetting(settings);

            //locales
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Fields.Url", "URL");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Fields.Url.Hint", "Specify Pakke URL.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Fields.AccessKey", "Access Key");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Fields.AccessKey.Hint", "Specify Pakke access key.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Fields.AdditionalHandlingCharge", "Additional handling charge");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Fields.AdditionalHandlingCharge.Hint", "Enter additional handling fee to charge your customers.");
            //tracker events
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.Departed", "Departed");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.ExportScanned", "Export scanned");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.OriginScanned", "Origin scanned");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.Arrived", "Arrived");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.NotDelivered", "Not delivered");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.Booked", "Booked");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.Delivered", "Delivered");
            //packing

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Fields.Tracing", "Tracing");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Pakke.Fields.Tracing.Hint", "Check if you want to record plugin tracing in System Log. Warning: The entire request and response XML will be logged (including AccessKey/UserName,Password). Do not leave this enabled in a production environment.");

            base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<PakkeSettings>();

            //locales
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Fields.Url");
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Fields.Url.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Fields.AccessKey");
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Fields.AccessKey.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Fields.AdditionalHandlingCharge");
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Fields.AdditionalHandlingCharge.Hint");

            //tracker events
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.Departed");
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.ExportScanned");
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.OriginScanned");
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.Arrived");
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.NotDelivered");
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.Booked");
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Tracker.Delivered");
            //packing
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Fields.Tracing");
            _localizationService.DeletePluginLocaleResource("Plugins.Shipping.Pakke.Fields.Tracing.Hint");

            base.Uninstall();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a shipping rate computation method type
        /// </summary>
        public ShippingRateComputationMethodType ShippingRateComputationMethodType
        {
            get { return ShippingRateComputationMethodType.Realtime; }
        }

        /// <summary>
        /// Gets a shipment tracker
        /// </summary>
        public IShipmentTracker ShipmentTracker
        {
            get { return new PakkeShipmentTracker(_logger, _localizationService, _pakkeSettings); }
        }

        #endregion
    }
}