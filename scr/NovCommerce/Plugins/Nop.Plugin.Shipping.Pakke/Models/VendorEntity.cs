﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Plugin.Shipping.Pakke.Models
{
    public class VendorEntity
    {

        /// <summary>
        /// Get same vendor count from list of vendors
        /// </summary>
        /// <param name="productValidation"></param>
        /// <param name="vendorId"></param>
        /// <returns></returns>
        public static int SameVendorCount(ProductValidation productValidation, int vendorId)
        {
            int sameVendorCount = 0;
            try
            {
                var totalVendors = productValidation.Vendors.GroupBy(x => x).
               Select(g => new { Vendor = g.Key, Count = g.Count() }).
               OrderByDescending(x => x.Count);

                foreach (var item in totalVendors)
                {
                    if (item.Vendor == vendorId)
                        sameVendorCount = item.Count;
                }
            }
            catch (Exception)
            {

                throw;
            }
           


            return sameVendorCount;
        }
    }
}
