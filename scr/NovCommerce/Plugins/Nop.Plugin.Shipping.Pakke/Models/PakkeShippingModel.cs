﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Shipping.Pakke.Models
{
    public class PakkeShippingModel : BaseNopModel
    {
        public PakkeShippingModel()
        {
       
        }
        [NopResourceDisplayName("Plugins.Shipping.Pakke.Fields.Url")]
        public string Url { get; set; }

 

        [NopResourceDisplayName("Plugins.Shipping.Pakke.Fields.AccessKey")]
        public string AccessKey { get; set; }

       

        [NopResourceDisplayName("Plugins.Shipping.Pakke.Fields.AdditionalHandlingCharge")]
        public decimal AdditionalHandlingCharge { get; set; }

        
 
 
      

        [NopResourceDisplayName("Plugins.Shipping.Pakke.Fields.Tracing")]
        public bool Tracing { get; set; }
        
    }
}