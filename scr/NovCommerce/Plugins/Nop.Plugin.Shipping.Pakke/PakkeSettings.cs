﻿using Nop.Core.Configuration;
using Nop.Plugin.Shipping.Pakke.Domain;

namespace Nop.Plugin.Shipping.Pakke
{
    /// <summary>
    /// Represents settings of the Pakke shipping plugin
    /// </summary>
    public class PakkeSettings : ISettings
    {
        /// <summary>
        /// Gets or sets UPS service URL
        /// </summary>
        public string Url { get; set; }

 

        /// <summary>
        /// Gets or sets the access key
        /// </summary>
        public string AccessKey { get; set; }

        

        /// <summary>
        /// Gets or sets an amount of the additional handling charge
        /// </summary>
        public decimal AdditionalHandlingCharge { get; set; }

         
       

        /// <summary>
        /// Gets or sets a value indicating whether to record plugin tracing in log
        /// </summary>
        public bool Tracing { get; set; }

         
    }
}