﻿using System;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Plugin.Shipping.Pakke.Domain;
using Nop.Plugin.Shipping.Pakke.Models;
using Nop.Services;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.Shipping.Pakke.Controllers
{
    [AuthorizeAdmin]
    [Area(AreaNames.Admin)]
    public class ShippingPakkeController : BasePluginController
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly PakkeSettings _pakkeSettings;

        #endregion

        #region Ctor

        public ShippingPakkeController(ILocalizationService localizationService,
            IPermissionService permissionService,
            ISettingService settingService,
            PakkeSettings pakkeSettings)
        {
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._settingService = settingService;
            this._pakkeSettings = pakkeSettings;
        }

        #endregion

        #region Methods

        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return AccessDeniedView();

            var model = new PakkeShippingModel
            {
                Url = _pakkeSettings.Url,
               
                AccessKey = _pakkeSettings.AccessKey,
               
                AdditionalHandlingCharge = _pakkeSettings.AdditionalHandlingCharge,
                Tracing = _pakkeSettings.Tracing,
                 
            };
           

          

            return View("~/Plugins/Shipping.Pakke/Views/Configure.cshtml", model);
        }

        [HttpPost]
        [AdminAntiForgery]
        public IActionResult Configure(PakkeShippingModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return AccessDeniedView();

            if (!ModelState.IsValid)
                return Configure();

            //save settings
            _pakkeSettings.Url = model.Url;
     
            _pakkeSettings.AccessKey = model.AccessKey;
         
            _pakkeSettings.AdditionalHandlingCharge = model.AdditionalHandlingCharge;
             
           
            _pakkeSettings.Tracing = model.Tracing;
    

            // Save selected services
            var carrierServicesOfferedDomestic = new StringBuilder();
            var carrierServicesDomesticSelectedCount = 0;
             
             

            _settingService.SaveSetting(_pakkeSettings);

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        #endregion
    }
}