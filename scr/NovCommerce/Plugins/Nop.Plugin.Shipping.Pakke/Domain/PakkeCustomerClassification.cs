﻿
namespace Nop.Plugin.Shipping.Pakke.Domain
{
    /// <summary>
    /// UPS customer Classification
    /// </summary>
    public enum PakkeCustomerClassification
    {
        /// <summary>
        /// Retail
        /// </summary>
        Retail,
        /// <summary>
        /// Wholesale
        /// </summary>
        Wholesale,
        /// <summary>
        /// Occasional
        /// </summary>
        Occasional
    }
}
