﻿
namespace Nop.Plugin.Shipping.Pakke.Domain
{
    /// <summary>
    /// Pakke pickup type
    /// </summary>
    public enum PakkePickupType
    {
        /// <summary>
        /// Daily pickup
        /// </summary>
        DailyPickup,
        /// <summary>
        /// Customer counter
        /// </summary>
        CustomerCounter,
        /// <summary>
        /// One time pickup
        /// </summary>
        OneTimePickup,
        /// <summary>
        /// On call air
        /// </summary>
        OnCallAir,
        /// <summary>
        /// Suggested retail rates
        /// </summary>
        SuggestedRetailRates,
        /// <summary>
        /// Letter center
        /// </summary>
        LetterCenter,
        /// <summary>
        /// Air service center
        /// </summary>
        AirServiceCenter
    }
}
