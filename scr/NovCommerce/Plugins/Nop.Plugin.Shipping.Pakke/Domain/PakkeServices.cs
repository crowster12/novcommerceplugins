﻿//------------------------------------------------------------------------------
// Contributor(s): mb 10/20/2010. 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Shipping.Pakke.Domain
{

    /// <summary>
    /// Class for UPS services
    /// </summary>
    public static class PakkeServices
    {

        private static readonly Dictionary<string, string> _services = new Dictionary<string, string>
        {
            {"UPS Next Day Air", "01"},
            {"UPS 2nd Day Air", "02"},
            {"UPS Ground", "03"},
            {"UPS Worldwide Express", "07"},
            {"UPS Worldwide Expedited", "08"},
            {"UPS Standard", "11"},
            {"UPS 3 Day Select", "12"},
            {"UPS Next Day Air Saver", "13"},
            {"UPS Next Day Air Early A.M.", "14"},
            {"UPS Worldwide Express Plus", "54"},
            {"UPS 2nd Day Air A.M.", "59"},
            {"UPS Saver", "65"},
            {"UPS Today Standard", "82"}, //82-86, for Polish Domestic Shipments
            {"UPS Today Dedicated Courier", "83"},
            {"UPS Today Express", "85"},
            {"UPS Today Express Saver", "86"},
            {"Saturday Delivery", "sa"},
        };

        #region Utilities
        /// <summary>
        /// This method calculate the volumetric weight
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static double CalculateVolumetricWeight(double width,double height,double length ){
            double volumetricWeight = 0;
            volumetricWeight = (width * height * length )/ 5000.00;
            return volumetricWeight;
        }

        /// <summary>
        /// This method validate the max dimension allowed per package 
        /// </summary>
        /// <param name="width">Current width of the package</param>
        /// <param name="height">Current height of the package</param>
        /// <param name="length">Current lenght of the package</param>
        /// <param name="maxWidth">Max width allowed</param>
        /// <param name="maxHeight">Max heigth allowed</param>
        /// <param name="maxLength">Max length allowed</param>
        /// <returns></returns>
        public static bool ValidateMaxDimensions(double width,double height, double length, double maxWidth,double maxHeight,double maxLength) {
            bool validateDimensions = false;
            if (width < maxWidth && height > maxHeight && length > maxLength)
                validateDimensions = true;
            else {
                validateDimensions = false;
            }
            return validateDimensions;
        }

        /// <summary>
        /// This method validate the min dimension allowed per package 
        /// </summary>
        /// <param name="width">Current width of the package</param>
        /// <param name="height">Current height of the package</param>
        /// <param name="length">Current lenght of the package</param>
        /// <param name="maxWidth">Min width allowed</param>
        /// <param name="maxHeight">Min heigth allowed</param>
        /// <param name="maxLength"> Min length allowed</param>
        /// <returns></returns>
        public static bool ValidateMinDimensions(double width, double height, double length, double minWidth, double minHeight, double minLength)
        {
            bool validateDimensions = false;
            if (width > minWidth && height > minHeight && length > minLength)
                validateDimensions = true;
            else
            {
                validateDimensions = false;
            }
            return validateDimensions;

        }

        /// <summary>
        /// Gets the Service ID for a service
        /// </summary>
        /// <param name="service">service name</param>
        /// <returns>service id or empty string if not found</returns>
        public static string GetServiceId(string service)
        {
            var serviceId = "";
            if (string.IsNullOrEmpty(service))
                return serviceId;

            if (_services.ContainsKey(service))
                serviceId = _services[service];
            return serviceId;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Pakke services string names
        /// </summary>
        public static string[] Services
        {
            get
            {
                return _services.Keys.Select(x => x).ToArray();
            }
        }
        #endregion


    }
}
