﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
 
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Text;
using Nop.Services.Customers;
using Nop.Web.Framework.Kendoui;
using System.Linq;
using Nop.Web.Framework.Mvc;
 
using System.Net;
using Nop.Plugin.Payments.OpenPayCards.Models;
using Nop.Plugin.Payments.OpenPayCards;

namespace Nop.Plugin.Payments.OpenPayCards.Controllers
{
    public class PaymentOpenPayCardsController: BasePaymentController
    {
        #region Fields
        private readonly IWorkContext _workContext;
        private readonly ISettingService _settingService;
        private readonly IPaymentService _paymentService;
        private readonly IOrderService _orderService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IPermissionService _permissionService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IStoreContext _storeContext;
        private readonly ILogger _logger;
        private readonly IWebHelper _webHelper;
        private readonly ShoppingCartSettings _shoppingCartSettings;
        private readonly OpenPayCardsSettings _openPayPaymentSettings;
       // private readonly OpenPayPaymentManager _openPayPaymentManager;
        private readonly ICustomerService _customerService;
        #endregion

        #region Ctor
        public PaymentOpenPayCardsController(ICustomerService customerService, ILocalizationService localizationService,
            ILogger logger,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IPermissionService permissionService,
            ISettingService settingService,
            IWebHelper webHelper,
            OpenPayCardsSettings openPayPaymentSettings )
        {
            this._customerService = customerService;
            this._localizationService = localizationService;
            this._logger = logger;
            this._orderProcessingService = orderProcessingService;
            this._orderService = orderService;
            this._permissionService = permissionService;
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._openPayPaymentSettings = openPayPaymentSettings;
        }

        #endregion

        #region Methods     
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();
            var model = new ConfigurationModel
            {
                MerchantId = _openPayPaymentSettings.MerchantId,
                PublicApiKey = _openPayPaymentSettings.PublicApiKey,
                PrivateApiKey = _openPayPaymentSettings.PrivateApiKey,

                UseSandbox = _openPayPaymentSettings.UseSandbox,
                AdditionalFee = _openPayPaymentSettings.AdditionalFee,
                AdditionalFeePercentage = _openPayPaymentSettings.AdditionalFeePercentage,
                TestShippingReference = _openPayPaymentSettings.TestShippingReference,
                VendorEmailFake = _openPayPaymentSettings.VendorEmailFake,
                CustomerEmailFake = _openPayPaymentSettings.CustomerEmailFake,
                SMTPSubjectPakke = _openPayPaymentSettings.SMTPSubjectPakke,
                MessagePakke = _openPayPaymentSettings.MessagePakke
            };
            return View("~/Plugins/Payments.OpenPayCards/Views/Configure.cshtml", model);
        }
        [HttpPost]
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();
            if (!ModelState.IsValid)
                return Configure();
            _openPayPaymentSettings.MerchantId = model.MerchantId;
            _openPayPaymentSettings.PublicApiKey = model.PublicApiKey;
            _openPayPaymentSettings.PrivateApiKey = model.PrivateApiKey;

            _openPayPaymentSettings.UseSandbox = model.UseSandbox;
            _openPayPaymentSettings.TestShippingReference = model.TestShippingReference;
            _openPayPaymentSettings.VendorEmailFake = model.VendorEmailFake;
            _openPayPaymentSettings.CustomerEmailFake = model.CustomerEmailFake;
            _openPayPaymentSettings.AdditionalFee = model.AdditionalFee;
            _openPayPaymentSettings.AdditionalFeePercentage = model.AdditionalFeePercentage;
            _openPayPaymentSettings.SMTPSubjectPakke = model.SMTPSubjectPakke;
            _openPayPaymentSettings.MessagePakke = model.MessagePakke;
            _settingService.SaveSetting(_openPayPaymentSettings);
            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));
            return Configure();
        }

        #endregion
    }

    public  class TestJson {

        public  bool Success { get; set; }
        public  string Result { get; set; }
        public  int ResponseCode { get; set; }
    }
}
