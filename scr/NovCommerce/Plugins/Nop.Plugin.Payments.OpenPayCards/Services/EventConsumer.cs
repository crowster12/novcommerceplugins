﻿using System.Linq;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Customers;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Payments;
 
using Nop.Web.Framework.Events;
using Nop.Web.Framework.Extensions;
using Nop.Web.Framework.UI;

namespace Nop.Plugin.Payments.OpenPayCards.Services
{
    public class EventConsumer :
        IConsumer<PageRenderingEvent>
    {
        #region Fields
        private readonly ICustomerService _customerService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IPaymentService _paymentService;
        #endregion
        #region Ctor
        public EventConsumer(ICustomerService customerService,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            IPaymentService paymentService )
        {
            this._customerService = customerService;
            this._genericAttributeService = genericAttributeService;
            this._localizationService = localizationService;
            this._paymentService = paymentService;
        }
        #endregion
        #region Methods
        /// <summary>
        /// Handle page rendering event
        /// </summary>
        /// <param name="eventMessage">Event message</param>
        public void HandleEvent(PageRenderingEvent eventMessage)
        {
            if (eventMessage?.Helper?.ViewContext?.ActionDescriptor == null)
                return;

            //check whether the payment plugin is installed and is active
            var openPayPaymentMethod = _paymentService.LoadPaymentMethodBySystemName(OpenPayCardsPaymentDefaults.SystemName);
            if (!(openPayPaymentMethod?.PluginDescriptor?.Installed ?? false) || !_paymentService.IsPaymentMethodActive(openPayPaymentMethod))
                return;
            //add js sсript to one page checkout
            if (eventMessage.GetRouteNames().Any(r => r.Equals("CheckoutOnePage")))
            {
                eventMessage.Helper.AddScriptParts(ResourceLocation.Footer, OpenPayCardsPaymentDefaults.PaymentScriptPathOpenPay2, excludeFromBundle:  false);
                eventMessage.Helper.AddScriptParts(ResourceLocation.Footer, OpenPayCardsPaymentDefaults.PaymentScriptPathOpenPay3, excludeFromBundle: false);
               // eventMessage.Helper.AddScriptParts(ResourceLocation.Footer, OpenPayCardsPaymentDefaults.PaymentScriptPathJquery, excludeFromBundle: false);
            }
        }

        #endregion
    }
}