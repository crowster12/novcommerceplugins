﻿ 

namespace Nop.Plugin.Payments.OpenPayCards.Model

{
    /// <summary>
    /// Represents base class for all openPay card requests.
    /// </summary>
    public abstract class OpenPayCardsRequest 
    {
        /// <summary>
        /// Get a request endpoint URL
        /// </summary>
        /// <returns>URL</returns>
        public abstract string GetRequestUrl();

        /// <summary>
        /// Get a request method
        /// </summary>
        /// <returns>Request method</returns>
        public abstract string GetRequestMethod();
    }
}
