﻿using FluentValidation.Attributes;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.OpenPayCards.Models
{
 
    public class ConfigurationModel : BaseNopModel
    {
        [NopResourceDisplayName("Plugins.Payments.OpenPayCards.Fields.MerchantId")]
        public string MerchantId { get; set; }

        [NopResourceDisplayName("Plugins.Payments.OpenPayCards.Fields.PublicApiKey")]
        public string PublicApiKey { get; set; }

        [NopResourceDisplayName("Plugins.Payments.OpenPayCards.Fields.PrivateApiKey")]
        public string PrivateApiKey { get; set; }




        [NopResourceDisplayName("Plugins.Payments.OpenPayCards.Fields.UseSandbox")]
        public bool UseSandbox { get; set; }

        [NopResourceDisplayName("Plugins.Payments.OpenPayCards.Fields.TestShippingReference")]
        public bool TestShippingReference { get; set; }

        [NopResourceDisplayName("Plugins.Payments.OpenPayCards.Fields.VendorEmailFake")]
        public string VendorEmailFake { get; set; }

        [NopResourceDisplayName("Plugins.Payments.OpenPayCards.Fields.CustomerEmailFake")]
        public string CustomerEmailFake { get; set; }



        [NopResourceDisplayName("Plugins.Payments.OpenPayCards.Fields.SMTPSubjectPakke")]
        public string SMTPSubjectPakke { get; set; }

        [NopResourceDisplayName("Plugins.Payments.OpenPayCards.Fields.MessagePakke")]
        public string MessagePakke { get; set; }


        [NopResourceDisplayName("Plugins.Payments.OpenPayCards.Fields.AdditionalFee")]
        public decimal AdditionalFee { get; set; }

        [NopResourceDisplayName("Plugins.Payments.OpenPayCards.Fields.AdditionalFeePercentage")]
        public bool AdditionalFeePercentage { get; set; }
    }
}
