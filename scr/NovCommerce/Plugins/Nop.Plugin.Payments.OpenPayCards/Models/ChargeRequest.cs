﻿using Newtonsoft.Json;
 
 
using Nop.Plugin.Payments.OpenPayCards.Domain.Models;
using Nop.Plugin.Payments.OpenPayCards.Model;
using Nop.Plugin.Payments.OpenPayCards.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Nop.Plugin.Payments.OpenPayCards.Domain.Requests
{
    public class ChargeRequest : OpenpayCardsPaymentRequest
    {
        #region Properties

        /// <summary>
        /// Gets or sets an amount of the charge to be authorized.
        /// </summary>
        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets a credit-card-specific data. 
        /// Required for credit card charges.
        /// </summary>
        [JsonProperty("card")]
        public Card Card { get; set; }

        /// <summary>
        /// Gets or sets a data from a Vault payment account. 
        /// Required for transactions where a Vault token is sent in the place of card or check data.
        /// </summary>
        [JsonProperty("paymentVaultToken")]
        public VaultToken PaymentVaultToken { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the card object is to be added to the vault to be stored as a token.
        /// </summary>
        [JsonProperty("addToVault")]
        public bool AddToVault { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the card object is to be added to the vault to be stored as a token even if the attempted authorization is declined.
        /// </summary>
        [JsonProperty("addToVaultOnFailure")]
        public bool AddToVaultOnFailure { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is permissible to authorize less than the total balance available on a prepaid card.
        /// </summary>
        [JsonProperty("allowPartialCharges")]
        public bool AllowPartialChanges { get; set; }

        /// <summary>
        /// Gets or sets an additional data to assist in reporting, ecommerce or moto transactions, and level 2 or level 3 processing. 
        /// Includes user-defined fields and invoice-related information.
        /// </summary>
        
       
      
        #endregion

        #region Methods

        /// <summary>
        /// Get a request endpoint URL
        /// </summary>
        /// <returns>URL</returns>
        public override string GetRequestUrl() => "api/Payments/Charge";

        /// <summary>
        /// Get a request method
        /// </summary>
        /// <returns>Request method</returns>
        public override string GetRequestMethod() => WebRequestMethods.Http.Post;

        #endregion
    }
}
