﻿using Microsoft.AspNetCore.Http;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Plugins;
using System;
using System.Collections.Generic;

using Nop.Plugin.Payments.OpenPay.Domain;
using Nop.Plugin.Payments.OpenPay.Domain.Requests;
using Nop.Plugin.Payments.OpenPay.Models;
using Nop.Services.Stores;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Orders;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Payments;
using Nop.Core.Domain.Directory;
using Nop.Plugin.Payments.OpenPay.Services;
using System.Linq;
using Nop.Plugin.Payments.OpenPay.Domain.Enums;
using Nop.Core.Domain.Payments;
using Nop.Services.Common;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Payments.OpenPay.Domain.Models;
using Nop.Services.Logging;
using Openpay;
using Openpay.Entities;
 
using Nop.Core.Domain.Messages;
using Nop.Services.Messages;

namespace Nop.Plugin.Payments.OpenPay
{
    public class OpenPayPaymentProcessor : BasePlugin, IPaymentMethod
    {
        #region Fields
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IEmailAccountService _emailAccountService;
        private readonly IQueuedEmailService _queuedEmailService;

        private readonly CurrencySettings _currencySettings;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerService _customerService;
        private readonly ICurrencyService _currencyService;
        private readonly IPaymentService _paymentService;
        private readonly IShoppingCartService _cartService;         
        private readonly ISettingService _settingService;
        private readonly ILogger _logger;
        private readonly IWebHelper _webHelper;
        private readonly OpenPaySettings _openPayPaymentSettings;
        private readonly IStoreService _storeService;
        private readonly OpenPayPaymentManager _openpayPaymentManager;
        private readonly IGenericAttributeService _genericAttributeService;
        #endregion

        #region Ctor

        public OpenPayPaymentProcessor(CurrencySettings currencySettings, ILocalizationService localizationService,
            ICurrencyService currencyService,
            ICustomerService customerService,
            IPaymentService paymentService,
            ISettingService settingService,
            IWebHelper webHelper,
            IStoreService storeService,             
            OpenPaySettings openPayPaymentSettings,
            IShoppingCartService cartService,
            EmailAccountSettings emailAccountSettings,
            IEmailAccountService emailAccountService ,
            IQueuedEmailService queuedEmailService)
        {
            this._queuedEmailService = queuedEmailService;
            this._emailAccountSettings = emailAccountSettings;
            this._emailAccountService = emailAccountService;
            this._customerService = customerService;
            this._settingService=settingService;
            this._storeService = storeService;
            this._localizationService = localizationService;
            this._paymentService = paymentService;
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._openPayPaymentSettings = openPayPaymentSettings;
            this._cartService = cartService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// This methog set the car info
        /// </summary>
        /// <returns></returns>
        public Openpay.Entities.Card GetCardInfo(ProcessPaymentRequest processPaymentRequest)
        {
            Openpay.Entities.Card card = new Openpay.Entities.Card();
            card.CardNumber = processPaymentRequest.CreditCardNumber;
            card.HolderName = processPaymentRequest.CreditCardName;// "Juanito Pérez Nuñez";
            card.Cvv2 = processPaymentRequest.CreditCardCvv2;
            card.ExpirationMonth = processPaymentRequest.CreditCardExpireMonth.ToString();
            card.ExpirationYear = processPaymentRequest.CreditCardExpireYear.ToString().Substring(2);
            //card.Type = "debit";
            //  card.ExpirationYear = DateTime.Now.AddYears(2).Year.ToString().Substring(2);
            return card;
        }

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {

            var result = new ProcessPaymentResult
            {
                AllowStoringCreditCardNumber = true
            };
            switch (_openPayPaymentSettings.TransactionMode)
            {
                case TransactionMode.Pending:
                    result.NewPaymentStatus = PaymentStatus.Pending;
                    break;
                case TransactionMode.Authorize:
                    result.NewPaymentStatus = PaymentStatus.Authorized;

                    OpenpayAPI openpayAPI = new OpenpayAPI(_openPayPaymentSettings.MerchantKey, _openPayPaymentSettings.MerchantId,!_openPayPaymentSettings.UseSandbox);

                    Openpay.Entities.Card card = openpayAPI.CardService.Create(GetCardInfo(processPaymentRequest));

                    //Put actually the openPay charge to merchant, but it can change with the follow tests
                    Openpay.Entities.Request.ChargeRequest request = new Openpay.Entities.Request.ChargeRequest();
                    
                    request.Method = "card";
                    request.SourceId = card.Id;
                    request.DeviceSessionId = processPaymentRequest.DeviceId;// "sah2e76qfdqa72ef2e2q";
         
                    request.Description = "Openpay con tarjeta de credito o debito";

                 
                    request.Amount = new Decimal(Convert.ToDouble(processPaymentRequest.OrderTotal));
                 

                    //get customer
                    var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);
                    if (customer == null)
                        throw new NopException("No se pudo cargar el cliente");

                    Openpay.Entities.Customer _customer = new Openpay.Entities.Customer();
                    _customer.Name = customer.Username;
                    _customer.LastName = customer.BillingAddress.LastName;
                    _customer.PhoneNumber = customer.BillingAddress.PhoneNumber; 
                    _customer.Email = customer.Email;

                    request.Customer = _customer;
 
                    Charge charge = openpayAPI.ChargeService.Create(request);


                    var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                    if (emailAccount == null)
                        emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
                    if (emailAccount == null)
                        throw new NopException("Email account can't be loaded");

                    throw new NopException("finished");//Remove after test


                    //This method will be after payment, and the result will be used to generete reference guides
                    // CompletePackageInformationForReferenceRate(packages);

                    break;
                //Charge AuthorizeAndCapture
                case TransactionMode.Charge:
                    result.NewPaymentStatus = PaymentStatus.Paid;
                    break;
                default:
                    result.AddError("Not supported transaction type");
                    break;
            }

            return result;
        }
        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/PaymentOpenPay/Configure";
        }

        public string GetPublicViewComponentName()
        {
            return "PaymentOpenPay";
        }

        /// <summary>
        /// Install the plugin
        /// </summary>
        public override void Install()
        {
            //settings
            _settingService.SaveSetting(new OpenPaySettings
            {
              //  UseSandbox = true
            });

            //locales
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPay.Fields.AdditionalFee", "Cuota Adicional");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPay.Fields.AdditionalFee.Hint", "Introduzca una tarifa adicional para cobrar a su clientes.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPay.Fields.AdditionalFeePercentage", "Cuota Adicional. Porcentaje de uso");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPay.Fields.AdditionalFeePercentage.Hint", "Determina si se aplica una tarifa adicional porcentual al total del pedido. Si no está habilitado, se usa un valor fijo.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPay.Fields.MerchantId", "Merchant ID");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPay.Fields.MerchantId.Hint", "Specificar merchant ID.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPay.Fields.MerchantKey", "Merchant key");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPay.Fields.MerchantKey.Hint", "Especificar clave de comerciante.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPay.Fields.UseSandbox", "Usar Sandbox");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPay.Fields.UseSandbox.Hint", "Marque para habilitar Sandbox (entorno de prueba).");
            //_localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPay.RedirectionTip", "Será redirigido al sitio de OpenPay para completar el pedido.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPay.PaymentMethodDescription", "Pago credito / tarjeta de debito.");

            base.Install();
        }

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<OpenPaySettings>();

            //locales
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPay.Fields.AdditionalFee");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPay.Fields.AdditionalFee.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPay.Fields.AdditionalFeePercentage");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPay.Fields.AdditionalFeePercentage.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPay.Fields.MerchantId");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPay.Fields.MerchantId.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPay.Fields.MerchantKey");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPay.Fields.MerchantKey.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPay.Fields.UseSandbox");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPay.Fields.UseSandbox.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPay.RedirectionTip");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPay.PaymentMethodDescription");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPay.OpenPayCustomer");

            base.Uninstall();
        }
     
         public bool SupportCapture
        {
            get { return true; }
        }
        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get { return true; }
        }
        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get { return RecurringPaymentType.Manual; }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public Nop.Services.Payments.PaymentMethodType PaymentMethodType
        {
            get { return Nop.Services.Payments.PaymentMethodType.Standard; }
        }

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo
        {
            get { return false; }
        }

        public string PaymentMethodDescription
        {
            get { return _localizationService.GetResource("Plugins.Payments.Openpay.PaymentMethodDescription"); }
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            //always success
            var result = new CancelRecurringPaymentResult();
            result.AddError("Pago recurrente no soportado");
            return result;
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            return order.OrderStatus == OrderStatus.Pending;
        }
        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            result.AddError("Método de captura no soportado");
            return result;
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            return _paymentService.CalculateAdditionalFee(cart,
                _openPayPaymentSettings.AdditionalFee, _openPayPaymentSettings.AdditionalFeePercentage);
        }

        public ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {
            return new ProcessPaymentRequest
            {
                CreditCardType = form["CardType"],
                CreditCardName = form["CardHolderName"],
                CreditCardNumber = form["CardNumber"],
                CreditCardExpireMonth = int.Parse(form["ExpireMonth"]),
                CreditCardExpireYear = int.Parse(form["ExpireYear"]),
                //DeviceId = form["DeviceId"],
                CreditCardCvv2 = form["CardCode"],
                DeviceId = form["DeviceId"]

            };
        }

        /// <summary>
        /// Returns a value indicating whether payment method should be hidden during checkout
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>true - hide; false - display.</returns>
        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            //you can put any logic here
            //for example, hide this payment method if all products in the cart are downloadable
            //or hide this payment method if current customer is from certain country
            return false;
        }
        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
           
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.AddError("Pago recurrente no soportado");
            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();
            result.AddError("Refund method not supported");
            return result;
        }

        public IList<string> ValidatePaymentForm(IFormCollection form)
        {
            return new List<string>();
        }


        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();
            result.AddError("Void method not supported");
            return result;
        }
        #endregion

        #region Utilities
        /// <summary>
        /// Process a regular or recurring payment
        /// </summary>
        /// <param name="paymentRequest">Payment request parameters</param>
        /// <param name="isRecurringPayment">Whether it is a recurring payment</param>
        /// <returns>Process payment result</returns>
        private ProcessPaymentResult ProcessPayment(ProcessPaymentRequest paymentRequest, bool isRecurringPayment)
        {

            //create charge request
            var chargeRequest = CreateChargeRequest(paymentRequest, isRecurringPayment);




            // var storeLocation = _webHelper.GetStoreLocation();


            //var pago = new Openpay.OpenpayAPI("sk_02a88126100d4a4482f493851d3f7089", "moy2ouzyuigxkkmspfbs", false);
            var pago = new Openpay.OpenpayAPI(_openPayPaymentSettings.MerchantKey, _openPayPaymentSettings.MerchantId, false);

            // Customer customer = new Customer();
            //  customer.Name = postProcessPaymentRequest.Order.Customer.Username;
            //customer.Email = postProcessPaymentRequest.Order.Customer.Email;

            // customer = pago.CustomerService.Create(customer);

            var request = CreateChargeRequest(paymentRequest, isRecurringPayment);

            //charge transaction
            var transaction = _openPayPaymentSettings.TransactionMode == TransactionMode.Authorize
                ? _openpayPaymentManager.Authorize(new AuthorizeRequest(request)) : _openpayPaymentManager.Charge(request)
                ?? throw new NopException("An error occurred while processing. Error details in the log");

            //save card identifier to payment custom values for further purchasing
            if (isRecurringPayment && !string.IsNullOrEmpty(transaction.VaultData?.Token?.PaymentMethodId))
                paymentRequest.CustomValues.Add(_localizationService.GetResource("Plugins.Payments.OpenPay.Fields.StoredCard.Key"), transaction.VaultData.Token.PaymentMethodId);

            //return result
            var result = new ProcessPaymentResult
            {
                AvsResult = $"{transaction.AvsResult}. Code: {transaction.AvsCode}",
                Cvv2Result = $"{transaction.CvvResult}. Code: {transaction.CvvCode}",
                AuthorizationTransactionCode = transaction.AuthorizationCode
            };

            if (_openPayPaymentSettings.TransactionMode == TransactionMode.Authorize)
            {
                result.AuthorizationTransactionId = transaction.TransactionId.ToString();
                result.AuthorizationTransactionResult = transaction.ResponseText;
                result.NewPaymentStatus = PaymentStatus.Authorized;
            }

            if (_openPayPaymentSettings.TransactionMode == TransactionMode.Charge)
            {
                result.CaptureTransactionId = transaction.TransactionId.ToString();
                result.CaptureTransactionResult = transaction.ResponseText;
                result.NewPaymentStatus = PaymentStatus.Paid;
            }

            return result;
        }


        /// <summary>
        /// Create request parameters to charge transaction
        /// </summary>
        /// <param name="paymentRequest">Payment request parameters</param>
        /// <param name="isRecurringPayment">Whether it is a recurring payment</param>
        /// <returns>Charge request parameters</returns>
        private ChargeRequest CreateChargeRequest(ProcessPaymentRequest paymentRequest, bool isRecurringPayment)
        {
            //get customer
            var customer = _customerService.GetCustomerById(paymentRequest.CustomerId);
            if (customer == null)
                throw new NopException("No se pudo cargar el cliente");

            //get the primary store currency
            var currency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
            if (currency == null)
                throw new NopException("La moneda de la tienda principal no se puede cargar");

            //Aqui podemos validar que moneda soporta nuestro openpay( pendiente)


            //Crear parámetros de solicitud de carga comunes
            var request = new ChargeRequest
            {
                OrderId = CommonHelper.EnsureMaximumLength(paymentRequest.OrderGuid.ToString(), 25),
                TransactionDuplicateCheckType = TransactionDuplicateCheckType.NoCheck,
                ExtendedInformation = new ExtendedInformation
                {
                    InvoiceNumber = paymentRequest.OrderGuid.ToString(),
                    InvoiceDescription = $"Order from the '{_storeService.GetStoreById(paymentRequest.StoreId)?.Name}'"
                }
            };

            //establecer la cantidad en USD
            var amount = _currencyService.ConvertFromPrimaryStoreCurrency(paymentRequest.OrderTotal, currency);
            
            request.Amount = Math.Round(amount, 2);

            //obtener carro de compras actual
            var shoppingCart = customer.ShoppingCartItems
                .Where(shoppingCartItem => shoppingCartItem.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(paymentRequest.StoreId).ToList();

            //si hay artículos no descargables en el carrito de compras
            var nonDownloadable = shoppingCart.Any(item => !item.Product.IsDownload);
            request.ExtendedInformation.GoodsType = nonDownloadable ? GoodsType.Physical : GoodsType.Digital;

            //Trate de obtener detalles de la tarjeta previamente almacenados
            var storedCardKey = _localizationService.GetResource("Plugins.Payments.OpenPay.Fields.StoredCard.Key");
            if (paymentRequest.CustomValues.TryGetValue(storedCardKey, out object storedCardId) && !storedCardId.ToString().Equals(Guid.Empty.ToString()))
            {
                //Compruebe si el cliente existe en Vault
                var vaultCustomer = _openpayPaymentManager.GetCustomer(_genericAttributeService.GetAttribute<string>(customer, OpenPayPaymentDefaults.CustomerIdAttribute))
                    ?? throw new NopException("Failed to retrieve customer");

                //usar la tarjeta previamente almacenada para cargar
                request.PaymentVaultToken = new VaultToken
                {
                    CustomerId = vaultCustomer.CustomerId,
                    PaymentMethodId = storedCardId.ToString()
                };

                return request;
            }

            //o intenta obtener el token de la tarjeta
            var cardTokenKey = _localizationService.GetResource("Plugins.Payments.OpenPay.Fields.Token.Key");
            if (!paymentRequest.CustomValues.TryGetValue(cardTokenKey, out object token) || string.IsNullOrEmpty(token?.ToString()))
                throw new NopException("Failed to get the card token");

            //retire el token de la tarjeta de los valores personalizados de pago, ya que ya no es necesario
            paymentRequest.CustomValues.Remove(cardTokenKey);

            //Se requiere clave pública para pagar con el token de la tarjeta.
            request.PaymentVaultToken = new VaultToken
            {
                PublicKey = _openPayPaymentSettings.MerchantKey //public key
            };

            //Si guardar detalles de la tarjeta para la compra futura
            var saveCardKey = _localizationService.GetResource("Plugins.Payments.Worldpay.Fields.SaveCard.Key");
            if (paymentRequest.CustomValues.TryGetValue(saveCardKey, out object saveCardValue) && saveCardValue is bool saveCard && saveCard && !customer.IsGuest())
            {
                //remove the value from payment custom values, since it is no longer needed
                paymentRequest.CustomValues.Remove(saveCardKey);

                try
                {
                    //check whether customer exists and try to create the new one, if not exists
                    var vaultCustomer = _openpayPaymentManager.GetCustomer(_genericAttributeService.GetAttribute<string>(customer, OpenPayPaymentDefaults.CustomerIdAttribute))
                        ?? _openpayPaymentManager.CreateCustomer(new CreateCustomerRequest
                        {
                            CustomerId = customer.Id.ToString(),
                            CustomerDuplicateCheckType = CustomerDuplicateCheckType.Ignore,
                            EmailReceiptEnabled = !string.IsNullOrEmpty(customer.Email),
                            Email = customer.Email,
                            FirstName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute),
                            LastName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute),
                            Company = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CompanyAttribute),
                            Phone = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute),
                            BillingAddress = new Domain.Models.Address
                            {
                                Line1 = customer.BillingAddress?.Address1,
                                City = customer.BillingAddress?.City,
                                State = customer.BillingAddress?.StateProvince?.Abbreviation,
                                Country = customer.BillingAddress?.Country?.TwoLetterIsoCode,
                                Zip = customer.BillingAddress?.ZipPostalCode,
                                Company = customer.BillingAddress?.Company,
                                Phone = customer.BillingAddress?.PhoneNumber
                            }
                        }) ?? throw new NopException("Failed to create customer. Error details in the log");

                    //save Vault customer identifier as a generic attribute
                    _genericAttributeService.SaveAttribute(customer, OpenPayPaymentDefaults.CustomerIdAttribute, vaultCustomer.CustomerId);

                    //add card to the Vault after charge
                    request.AddToVault = true;
                    request.PaymentVaultToken.CustomerId = vaultCustomer.CustomerId;
                }
                catch (Exception exception)
                {
                    _logger.Warning(exception.Message, exception, customer);
                    if (isRecurringPayment)
                        throw new NopException("For recurring payments you need to save the card details");
                }
            }
            else if (isRecurringPayment)
                throw new NopException("For recurring payments you need to save the card details");

            //use card token to charge
            request.PaymentVaultToken.PaymentMethodId = token.ToString();

            return request;
        }
        #endregion

    }
}
