﻿using System.Runtime.Serialization;

namespace Nop.Plugin.Payments.OpenPay.Domain.Enums
{
    /// <summary>
    /// Enumeración de tipo de bienes. Indica el tipo de bienes que se están comprando.
    /// </summary>
    public enum GoodsType
    {
        /// <summary>
        /// Digital
        /// </summary>
        [EnumMember(Value = "DIGITAL")]
        Digital,

        /// <summary>
        /// Physical
        /// </summary>
        [EnumMember(Value = "PHYSICAL")]
        Physical
    }
}