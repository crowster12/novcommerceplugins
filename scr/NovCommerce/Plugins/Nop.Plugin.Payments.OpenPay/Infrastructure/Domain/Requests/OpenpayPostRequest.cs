﻿using Newtonsoft.Json;
using Nop.Plugin.Payments.OpenPay.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.OpenPay.Domain.Requests
{
    public abstract class OpenpayPostRequest: OpenPayRequest
    {
        /// <summary>
        /// Gets or sets a developer Id and version information related to the integration.
        /// </summary>
        [JsonProperty("developerApplication")]
        public DeveloperApplication DeveloperApplication { get; set; }
    }
}
