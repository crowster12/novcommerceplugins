﻿ 

namespace Nop.Plugin.Payments.OpenPay.Domain.Requests
{
    /// <summary>
    /// Represents base class for all openPay requests.
    /// </summary>
    public abstract class OpenPayRequest 
    {
        /// <summary>
        /// Get a request endpoint URL
        /// </summary>
        /// <returns>URL</returns>
        public abstract string GetRequestUrl();

        /// <summary>
        /// Get a request method
        /// </summary>
        /// <returns>Request method</returns>
        public abstract string GetRequestMethod();
    }
}
