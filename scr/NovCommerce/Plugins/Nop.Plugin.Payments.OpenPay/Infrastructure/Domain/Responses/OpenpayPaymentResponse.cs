﻿using Newtonsoft.Json;
using Nop.Plugin.Payments.OpenPay.Domain.Models;
using System.Collections.Generic;
using System.Text;
 

namespace Nop.Plugin.Payments.OpenPay.Domain.Responses
{
    public class OpenpayPaymentResponse : OpenPayResponse
    {
        /// <summary>
        /// Gets or sets a detailed information about the transaction, including but not limited to: transaction id; authorization code; avs result code; and cvv result code.
        /// </summary>
        [JsonProperty("transaction")]
        public Transaction Transaction { get; set; }
    }
}
