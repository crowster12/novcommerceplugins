﻿
namespace Nop.Plugin.Payments.OpenPay.Domain.Responses
{
    /// <summary>
    /// Represents return values of delete card requests
    /// </summary>
    public class DeleteCardResponse : OpenPayResponse
    {
    }
}