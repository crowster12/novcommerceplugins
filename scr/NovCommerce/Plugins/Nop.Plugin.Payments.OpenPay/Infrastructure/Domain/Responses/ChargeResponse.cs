﻿
namespace Nop.Plugin.Payments.OpenPay.Domain.Responses
{
    /// <summary>
    /// Represents return values of charge requests
    /// </summary>
    public class ChargeResponse : OpenpayPaymentResponse
    {
    }
}