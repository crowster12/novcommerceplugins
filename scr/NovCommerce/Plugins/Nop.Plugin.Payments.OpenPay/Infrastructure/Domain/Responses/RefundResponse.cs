﻿
using Nop.Plugin.Payments.OpenPay.Domain.Responses;

namespace Nop.Plugin.Payments.OpenPay.Domain.Responses
{
    /// <summary>
    /// Represents return values of refund requests
    /// </summary>
    public class RefundResponse : OpenpayPaymentResponse
    {
    }
}