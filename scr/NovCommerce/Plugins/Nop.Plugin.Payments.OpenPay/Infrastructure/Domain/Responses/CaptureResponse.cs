﻿ 

namespace Nop.Plugin.Payments.OpenPay.Domain.Responses
{
    /// <summary>
    /// Represents return values of capture requests
    /// </summary>
    public class CaptureResponse : OpenpayPaymentResponse
    {
    }
}