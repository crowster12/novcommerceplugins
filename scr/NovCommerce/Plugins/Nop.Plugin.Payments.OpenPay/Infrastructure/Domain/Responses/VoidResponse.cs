﻿
using Nop.Plugin.Payments.OpenPay.Domain.Responses;

namespace Nop.Plugin.Payments.OpenPay.Domain.Responses
{
    /// <summary>
    /// Represents return values of void requests
    /// </summary>
    public class VoidResponse : OpenpayPaymentResponse
    {
    }
}