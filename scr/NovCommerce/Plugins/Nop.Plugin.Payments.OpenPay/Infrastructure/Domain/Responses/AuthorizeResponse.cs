﻿
 

namespace Nop.Plugin.Payments.OpenPay.Domain.Responses
{
    /// <summary>
    /// Represents return values of authorization requests
    /// </summary>
    public class AuthorizeResponse : OpenpayPaymentResponse
    {
    }
}