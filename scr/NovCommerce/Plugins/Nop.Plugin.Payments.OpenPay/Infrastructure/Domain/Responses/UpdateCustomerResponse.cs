﻿
namespace Nop.Plugin.Payments.OpenPay.Domain.Responses
{
    /// <summary>
    /// Represents return values of update customer requests
    /// </summary>
    public class UpdateCustomerResponse : GetCustomerResponse
    {
    }
}