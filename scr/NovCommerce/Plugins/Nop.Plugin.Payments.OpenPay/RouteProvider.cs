﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework.Mvc.Routing;

namespace Nop.Plugin.Payments.OpenPay
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Plugin.Payments.OpenPay.Rebilling",
                 "Plugins/PaymentOpenPay/Rebilling",
                 new { controller = "PaymentOpenPay", action = "Rebilling" });
        }

        public int Priority
        {
            get { return 0; }
        }
    }
}
