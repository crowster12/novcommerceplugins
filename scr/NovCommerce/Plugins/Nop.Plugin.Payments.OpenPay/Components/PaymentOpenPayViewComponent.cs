﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Payments.OpenPay.Domain.Enums;
using Nop.Plugin.Payments.OpenPay.Models;
using Nop.Plugin.Payments.OpenPay.Services;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Plugin.Payments.OpenPay.Components
{
 
    [ViewComponent(Name = OpenPayPaymentDefaults.ViewComponentName)]
    public class PaymentOpenPayViewComponent : ViewComponent
    {
        #region Fields

        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
 

        #endregion


        #region Ctor

        public PaymentOpenPayViewComponent(IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            IWorkContext workContext )
        {
            this._genericAttributeService = genericAttributeService;
            this._localizationService = localizationService;
            this._workContext = workContext;
 
        }

        #endregion


        public IViewComponentResult Invoke()
        {
            var model = new PaymentInfoModel();

            //prepare years
            for (var i = 0; i < 15; i++)
            {
                var year = (DateTime.Now.Year + i).ToString();
                model.ExpireYears.Add(new SelectListItem { Text = year, Value = year, });
            }

            //prepare months
            for (var i = 1; i <= 12; i++)
            {
                model.ExpireMonths.Add(new SelectListItem { Text = i.ToString("D2"), Value = i.ToString(), });
            }

            //prepare card types
            model.CardTypes = Enum.GetValues(typeof(CreditCardType)).OfType<CreditCardType>().Select(cardType => new SelectListItem
            {
                Value = JsonConvert.SerializeObject(cardType, new StringEnumConverter()),
                Text = _localizationService.GetLocalizedEnum(cardType)
            }).ToList();

            //whether current customer is guest
           /* model.IsGuest = _workContext.CurrentCustomer.IsGuest();
            if (!model.IsGuest)
            {
                //whether customer already has stored cards
                var customer = _openPayPaymentManager.GetCustomer(_genericAttributeService.GetAttribute<string>(_workContext.CurrentCustomer, OpenPayPaymentDefaults.CustomerIdAttribute));
                if (customer?.PaymentMethods != null)
                {
                    model.StoredCards = customer.PaymentMethods.Where(method => method?.Card != null)
                        .Select(method => new SelectListItem { Text = method.Card.MaskedNumber, Value = method.PaymentId }).ToList();
                }

                //add the special item for 'select card' with empty GUID value 
                if (model.StoredCards.Any())
                {
                    var selectCardText = _localizationService.GetResource("Plugins.Payments.OpenPay.Fields.StoredCard.SelectCard");
                    model.StoredCards.Insert(0, new SelectListItem { Text = selectCardText, Value = Guid.Empty.ToString() });
                }
            }*/
            return View("~/Plugins/Payments.OpenPay/Views/PaymentInfo.cshtml",model);
        }
    }
}
