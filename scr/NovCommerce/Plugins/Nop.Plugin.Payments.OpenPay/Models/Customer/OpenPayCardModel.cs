﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Payments.OpenPay.Models.Customer
{
    public class OpenPayCardModel : BaseNopModel
    {
        public string Id { get; set; }

        public string CardId { get; set; }
        
        public string MaskedNumber { get; set; }

        public string ExpirationDate { get; set; }
        
        public string CardType { get; set; }
    }
}