﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.OpenPay.Models;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Text;

using Nop.Services.Customers;
using Nop.Web.Framework.Kendoui;
using Nop.Plugin.Payments.OpenPay.Domain.Requests;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Payments.OpenPay.Domain.Enums;
using Nop.Plugin.Payments.OpenPay.Domain.Models;
using Nop.Plugin.Payments.OpenPay.Services;
using Nop.Web.Areas.Admin.Controllers;
using System.Linq;
using Nop.Web.Framework.Mvc;
using Nop.Plugin.Payments.OpenPay.Models.Customer;
using System.Net;

namespace Nop.Plugin.Payments.OpenPay.Controllers
{
    public class PaymentOpenPayController: BasePaymentController
    {
        #region Fields
        private readonly IWorkContext _workContext;
        private readonly ISettingService _settingService;
        private readonly IPaymentService _paymentService;
        private readonly IOrderService _orderService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IPermissionService _permissionService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IStoreContext _storeContext;
        private readonly ILogger _logger;
        private readonly IWebHelper _webHelper;
        private readonly ShoppingCartSettings _shoppingCartSettings;
        private readonly OpenPaySettings _openPayPaymentSettings;
        private readonly OpenPayPaymentManager _openPayPaymentManager;
        private readonly ICustomerService _customerService;
        #endregion

        #region Ctor
        public PaymentOpenPayController(ICustomerService customerService, ILocalizationService localizationService,
            ILogger logger,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IPermissionService permissionService,
            ISettingService settingService,
            IWebHelper webHelper,
            OpenPaySettings openPayPaymentSettings )
        {
            this._customerService = customerService;
            this._localizationService = localizationService;
            this._logger = logger;
            this._orderProcessingService = orderProcessingService;
            this._orderService = orderService;
            this._permissionService = permissionService;
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._openPayPaymentSettings = openPayPaymentSettings;
      
        }

        #endregion

        #region Methods     
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            var model = new ConfigurationModel
            {
                MerchantId = _openPayPaymentSettings.MerchantId,
                MerchantKey = _openPayPaymentSettings.MerchantKey,
                UseSandbox = _openPayPaymentSettings.UseSandbox,
                AdditionalFee = _openPayPaymentSettings.AdditionalFee,
                AdditionalFeePercentage = _openPayPaymentSettings.AdditionalFeePercentage

            };

            return View("~/Plugins/Payments.OpenPay/Views/Configure.cshtml", model);
        }

        [HttpPost]
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();
            if (!ModelState.IsValid)
                return Configure();

            _openPayPaymentSettings.MerchantId = model.MerchantId;
            _openPayPaymentSettings.MerchantKey = model.MerchantKey;
            _openPayPaymentSettings.UseSandbox = model.UseSandbox;
            _openPayPaymentSettings.AdditionalFee = model.AdditionalFee;
            _openPayPaymentSettings.AdditionalFeePercentage = model.AdditionalFeePercentage;

            _settingService.SaveSetting(_openPayPaymentSettings);

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }



     
        [HttpPost]
        public IActionResult CardList(int customerId)
        {
            //whether user has the authority
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedKendoGridJson();

            //check whether customer exists
            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                throw new ArgumentException("No se encontro el cliente con el id especificado ", nameof(customerId));

            //try to get stored credit cards of the Vault customer
            var storedCards = _openPayPaymentManager.GetCustomer(_genericAttributeService.GetAttribute<string>(customer, OpenPayPaymentDefaults.CustomerIdAttribute))?
                .PaymentMethods?.Where(method => method?.Card != null).ToList() ?? new List<PaymentMethod>();

            //prepare grid model
            var gridModel = new DataSourceResult
            {
                Data = storedCards.Select(card => new OpenPayCardModel
                {
                    Id = card.PaymentId,
                    CardId = card.PaymentId,
                    CardType = card.Card.CreditCardType.HasValue ? _localizationService.GetLocalizedEnum(card.Card.CreditCardType.Value) : null,
                    ExpirationDate = card.Card.ExpirationDate,
                    MaskedNumber = card.Card.MaskedNumber
                }),
                Total = storedCards.Count
            };

            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult CardDelete(string id, int customerId)
        {
            //whether user has the authority
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            //check whether customer exists
            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                throw new ArgumentException("No se encontro el cliente con el id especificado : ", nameof(customerId));

            //try to delete selected card from the Vault
            var deleted = _openPayPaymentManager.Deletecard(new DeleteCardRequest
            {
                CustomerId = _genericAttributeService.GetAttribute<string>(customer, OpenPayPaymentDefaults.CustomerIdAttribute),
                PaymentId = id
            });
            if (!deleted)
                throw new NopException("OpenPay Vault error: Fallo al eliminar la tarjeta. Detalles de errror en el log");

            return new NullJsonResult();
        }




        [HttpPost]
        // public ActionResult Rebilling(IpnModel model)
          public JsonResult Rebilling(string model)

        {
            var parameters = model;
            TestJson testJson = new TestJson();
            testJson.Success = true;
            testJson.Result = "Ok";
            testJson.ResponseCode = 200;

            return Json(testJson );
        }




        #endregion
    }

    public  class TestJson {

        public  bool Success { get; set; }
        public  string Result { get; set; }
        public  int ResponseCode { get; set; }
    }
}
