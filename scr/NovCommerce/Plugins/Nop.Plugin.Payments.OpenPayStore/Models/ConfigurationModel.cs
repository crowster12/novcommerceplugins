﻿using FluentValidation.Attributes;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.OpenPayStore.Models
{
 
    public class ConfigurationModel : BaseNopModel
    {
        [NopResourceDisplayName("Plugins.Payments.OpenPayStore.Fields.MerchantId")]
        public string MerchantId { get; set; }

        [NopResourceDisplayName("Plugins.Payments.OpenPayStore.Fields.MerchantKey")]
        public string MerchantKey { get; set; }

        [NopResourceDisplayName("Plugins.Payments.OpenPayStore.Fields.UseSandbox")]
        public bool UseSandbox { get; set; }

        [NopResourceDisplayName("Plugins.Payments.OpenPayStore.Fields.TestShippingReference")]
        public bool TestShippingReference { get; set; }

        //[NopResourceDisplayName("Plugins.Payments.OpenPayStore.Fields.VendorEmailFake")]
        //public string VendorEmailFake { get; set; }
        //[NopResourceDisplayName("Plugins.Payments.OpenPayStore.Fields.CustomerEmailFake")]
        //public string CustomerEmailFake { get; set; }

        [NopResourceDisplayName("Plugins.Payments.OpenPayStore.Fields.AdditionalFee")]
        public decimal AdditionalFee { get; set; }

        [NopResourceDisplayName("Plugins.Payments.OpenPayStore.Fields.AdditionalFeePercentage")]
        public bool AdditionalFeePercentage { get; set; }
 
        [NopResourceDisplayName("Plugins.Payments.OpenPayStore.Fields.SMTPSubjectOpenPay")]
        public string SMTPSubjectOpenPay { get; set; }
        [NopResourceDisplayName("Plugins.Payments.OpenPayStore.Fields.MessageOpenPay")]
        public string MessageOpenPay { get; set; }

        //[NopResourceDisplayName("Plugins.Payments.OpenPayStore.Fields.SMTPSubjectPakke")]
        //public string SMTPSubjectPakke { get; set; }
        //[NopResourceDisplayName("Plugins.Payments.OpenPayStore.Fields.MessagePakke")]
        //public string MessagePakke { get; set; }

    }
}
