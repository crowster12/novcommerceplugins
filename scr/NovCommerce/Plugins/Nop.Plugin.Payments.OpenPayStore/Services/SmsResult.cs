﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.OpenPayStore.Services
{
    public class SmsResult
    {
        private int returnCode;

        public int ReturnCode
        {
            get { return returnCode; }
            set { returnCode = value; }
        }

        private string returnMessage;

        public string ReturnMessage
        {
            get { return returnMessage; }
            set { returnMessage = value; }
        }

        private string stackTrace;

        public string StackTrace
        {
            get { return stackTrace; }
            set { stackTrace = value; }
        }
        private string messageId;

        public string MessageId
        {
            get { return messageId; }
            set { messageId = value; }
        }

        private int caseMessageId;

        public int CaseMessageId
        {
            get { return caseMessageId; }
            set { caseMessageId = value; }
        }
        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private string phoneMessage;

        public string PhoneMessage
        {
            get { return phoneMessage; }
            set { phoneMessage = value; }
        }
        //SMTP roperties

        public string SMTPReplyTo { get; set; }

        public string SMTPMessage { get; set; }



        public SmsResult()
        {
            this.caseMessageId = 0;
            this.returnCode = 0;
            this.messageId = "";
            this.returnMessage = "Success!!";
            this.stackTrace = "";
            this.email = "";
            this.phoneMessage = "";

            //SMTP initialization

            this.SMTPReplyTo = "";
            this.SMTPMessage = "";
        }
    }
}
