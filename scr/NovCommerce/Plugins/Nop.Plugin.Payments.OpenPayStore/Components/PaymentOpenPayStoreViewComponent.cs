﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Payments.OpenPayStore.Models;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Plugin.Payments.OpenPayStore.Components
{
 
    [ViewComponent(Name = OpenPayStorePaymentDefaults.ViewComponentName)]
    public class PaymentOpenPayStoreViewComponent : ViewComponent
    {
        #region Fields
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        #endregion
        #region Ctor

        public PaymentOpenPayStoreViewComponent(IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            IWorkContext workContext )
        {
            this._genericAttributeService = genericAttributeService;
            this._localizationService = localizationService;
            this._workContext = workContext;
        }

        #endregion
        public IViewComponentResult Invoke()
        {
            var model = new PaymentInfoModel();

           
            return View("~/Plugins/Payments.OpenPayStore/Views/PaymentInfo.cshtml",model);
        }
    }
}
