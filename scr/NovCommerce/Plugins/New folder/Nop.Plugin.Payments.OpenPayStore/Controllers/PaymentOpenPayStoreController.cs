﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
 
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Text;
using Nop.Services.Customers;
using Nop.Web.Framework.Kendoui; 
using Nop.Core.Domain.Customers; 
using Nop.Web.Areas.Admin.Controllers;
using System.Linq;
using Nop.Web.Framework.Mvc;
 
using System.Net;
using Nop.Plugin.Payments.OpenPayStore.Models;

namespace Nop.Plugin.Payments.OpenPayStore.Controllers
{
    public class PaymentOpenPayStoreController : BasePaymentController
    {
        #region Fields
        private readonly IWorkContext _workContext;
        private readonly ISettingService _settingService;
        private readonly IPaymentService _paymentService;
        private readonly IOrderService _orderService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IPermissionService _permissionService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IStoreContext _storeContext;
        private readonly ILogger _logger;
        private readonly IWebHelper _webHelper;
        private readonly ShoppingCartSettings _shoppingCartSettings;
        private readonly OpenPayStoreSettings _openPayStorePaymentSettings;

        private readonly ICustomerService _customerService;
        #endregion

        #region Ctor
        public PaymentOpenPayStoreController(ICustomerService customerService, ILocalizationService localizationService,
            ILogger logger,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IPermissionService permissionService,
            ISettingService settingService,
            IWebHelper webHelper,
            OpenPayStoreSettings openPayStorePaymentSettings)
        {
            this._customerService = customerService;
            this._localizationService = localizationService;
            this._logger = logger;
            this._orderProcessingService = orderProcessingService;
            this._orderService = orderService;
            this._permissionService = permissionService;
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._openPayStorePaymentSettings = openPayStorePaymentSettings;

        }

        #endregion

        #region Methods     
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            var model = new ConfigurationModel
            {
                MerchantId = _openPayStorePaymentSettings.MerchantId,
                MerchantKey = _openPayStorePaymentSettings.MerchantKey,
                UseSandbox = _openPayStorePaymentSettings.UseSandbox,
                //TestShippingReference= _openPayStorePaymentSettings.TestShippingReference,
                //CustomerEmailFake= _openPayStorePaymentSettings.CustomerEmailFake,
                //VendorEmailFake= _openPayStorePaymentSettings.VendorEmailFake,

                AdditionalFee = _openPayStorePaymentSettings.AdditionalFee,
                AdditionalFeePercentage = _openPayStorePaymentSettings.AdditionalFeePercentage,
          
                SMTPSubjectOpenPay= _openPayStorePaymentSettings.SMTPSubjectOpenPay,
                MessageOpenPay= _openPayStorePaymentSettings.MessageOpenPay,

                //SMTPSubjectPakke = _openPayStorePaymentSettings.SMTPSubjectPakke,
                //MessagePakke = _openPayStorePaymentSettings.MessagePakke


            };

            return View("~/Plugins/Payments.OpenPayStore/Views/Configure.cshtml", model);
        }

        [HttpPost]
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();
            if (!ModelState.IsValid)
                return Configure();
            _openPayStorePaymentSettings.MerchantId = model.MerchantId;
            _openPayStorePaymentSettings.MerchantKey = model.MerchantKey;
            _openPayStorePaymentSettings.UseSandbox = model.UseSandbox;
            //_openPayStorePaymentSettings.TestShippingReference = model.TestShippingReference;
            //_openPayStorePaymentSettings.CustomerEmailFake = model.CustomerEmailFake;
            //_openPayStorePaymentSettings.VendorEmailFake = model.VendorEmailFake;
            _openPayStorePaymentSettings.AdditionalFee = model.AdditionalFee;
            _openPayStorePaymentSettings.AdditionalFeePercentage = model.AdditionalFeePercentage;           
            _openPayStorePaymentSettings.SMTPSubjectOpenPay = model.SMTPSubjectOpenPay; 
            _openPayStorePaymentSettings.MessageOpenPay = model.MessageOpenPay;
            //_openPayStorePaymentSettings.SMTPSubjectPakke = model.SMTPSubjectPakke;
            //_openPayStorePaymentSettings.MessagePakke = model.MessagePakke;
            _settingService.SaveSetting(_openPayStorePaymentSettings);
            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));
            return Configure();
        }
        #endregion
    }
}
 
 