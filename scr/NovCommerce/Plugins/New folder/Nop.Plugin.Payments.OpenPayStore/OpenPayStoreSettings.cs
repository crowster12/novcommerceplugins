﻿using Nop.Core.Configuration;
using Nop.Plugin.Payments.OpenPayStore.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.OpenPayStore
{
    public class OpenPayStoreSettings : ISettings
    {
        /// <summary>
        /// Gets or sets merchant ID
        /// </summary>
        public string MerchantId { get; set; }

        /// <summary>
        /// Gets or sets merchant key
        /// </summary>
        public string MerchantKey { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use sandbox (testing environment)
        /// </summary>
        public bool UseSandbox { get; set; }
        /// <summary>
        /// This value enable the test to send reference message
        /// </summary>
        //public bool TestShippingReference { get; set; }
        ///// <summary>
        ///// Email fake to  send the reference
        ///// </summary>
        //public string VendorEmailFake { get; set; }

        ///// <summary>
        ///// Email fake to tes send the reference
        ///// </summary>
        //public string CustomerEmailFake { get; set; }


        /// <summary>
        /// Gets or sets an additional fee
        /// </summary>
        public decimal AdditionalFee { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to "additional fee" is specified as percentage. true - percentage, false - fixed value.
        /// </summary>
        public bool AdditionalFeePercentage { get; set; }


       
        public string SMTPSubjectOpenPay { get; set; }
        //public string SMTPSubjectPakke { get; set; }

        /// <summary>
        /// Sample message to send to the customers
        /// </summary>
        public string MessageOpenPay { get; set; }

        //public string MessagePakke { get; set; }

    }
}
