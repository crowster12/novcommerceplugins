﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Nop.Plugin.Payments.OpenPayStore.Services
{
    public class SMTPProvider
    {
        #region Events and Delegates to 
        public delegate void SmsErrorEventHandler(SmsResult error);
        public delegate void SmsSuccessEventHandler(SmsResult message);
        public event SmsErrorEventHandler ErrNoSent;
        public event SmsSuccessEventHandler MsgSent;

        #endregion
        #region Variable and Properties
        SmsResult result;

        private string _SMTPHost;

        public string SMTPHost
        {
            get { return _SMTPHost; }
            set { _SMTPHost = value; }
        }
        private string _SMTPport;

        public string SMTPPort
        {
            get { return _SMTPport; }
            set { _SMTPport = value; }
        }

        private string _SMTPUserName;

        public string SMTPUserName
        {
            get { return _SMTPUserName; }
            set { _SMTPUserName = value; }
        }
        private string _SMTPPassword;

        public string SMTPPassword
        {
            get { return _SMTPPassword; }
            set { _SMTPPassword = value; }
        }

        private string _Subject;

        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; }
        }

        private string _Message;

        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        private string _ReplyTo;

        public string ReplyTo
        {
            get { return _ReplyTo; }
            set { _ReplyTo = value; }
        }


        #endregion

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="_replyTo">this is the email to send</param>
        /// <param name="_message">Message to send to replyTo</param>
        public SMTPProvider(string SMTPHost, string SMTPPort, string SMTPUserName, string SMTPPassword, string Subject, string Message, string ReplyTo)
        {
            try
            {
                // Initialize properties
                this.SMTPHost = SMTPHost;
                this.SMTPPort = SMTPPort;
                this.SMTPUserName = SMTPUserName;
                this.SMTPPassword = SMTPPassword;
                this.Subject = Subject;
                this.Message = Message;
                this.ReplyTo = ReplyTo;

                //Initiliaze the result
                result = new SmsResult
                {
                    
                };
            }
            catch (Exception ex)
            {
                //log.Fatal(message: "Constructor SMTPProvider, Initialize Error ", exception: ex);
                //Send back a result 
                result.ReturnCode = -1;
                result.ReturnMessage = ex.Message;
                result.StackTrace = ex.StackTrace;
                ErrNoSent.Invoke(result);
            }
        }
        /// <summary>
        /// This method create the smtp client to send emails
        /// </summary>
        public void SendEmail()
        {
            try
            {
                var client = new SmtpClient(SMTPHost, Convert.ToInt32(SMTPPort))
                {
                    Credentials = new NetworkCredential(SMTPUserName, SMTPPassword),
                    EnableSsl = true
                };
                MailMessage message = new MailMessage(SMTPUserName, ReplyTo, Subject, Message);
                message.IsBodyHtml = true;
                client.Send(message);
                
            }
            catch (Exception ex)
            {
                //log.Fatal(message: "Send Email Error ", exception: ex);
                //Send back a result 
                result.ReturnCode = -1;
                result.ReturnMessage = ex.Message;
                result.StackTrace = ex.StackTrace;
                
            }
        }
    }
}
