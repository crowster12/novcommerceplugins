﻿using Microsoft.AspNetCore.Http;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Plugins;
using System;
using System.Collections.Generic;
using Nop.Services.Stores;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Payments;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Payments;
using Nop.Services.Common;
using Nop.Services.Logging;
using Nop.Plugin.Payments.OpenPayStore;
using Nop.Plugin.Payments.OpenPayStore.Domain.Enums;
using Openpay;
using Openpay.Entities;
using Nop.Plugin.Payments.OpenPayStore.Services;
using Nop.Core.Infrastructure;
using Nop.Core.Http.Extensions;
using Nop.Core.Domain.Messages;
using Nop.Services.Messages;
using Nop.Services.Media;
using System.Linq;

namespace Nop.Plugin.Payments.OpenPay
{
    public class OpenPayStorePaymentProcessor : BasePlugin, IPaymentMethod
    {
        #region Fields
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IEmailAccountService _emailAccountService;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailSender _emailSender;
        private readonly IAddressService _addressService;
        private readonly IDownloadService _downloadService;

        private readonly CurrencySettings _currencySettings;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerService _customerService;
        private readonly ICurrencyService _currencyService;
        private readonly IPaymentService _paymentService;
        private readonly ISettingService _settingService;
        private readonly ILogger _logger;
        private readonly IWebHelper _webHelper;
        private readonly OpenPayStoreSettings _openPayStorePaymentSettings;
        private readonly IStoreService _storeService;
        private readonly IGenericAttributeService _genericAttributeService;
        #endregion

        #region Ctor

        public OpenPayStorePaymentProcessor(CurrencySettings currencySettings, ILocalizationService localizationService,
            ICurrencyService currencyService,
            ICustomerService customerService,
            IPaymentService paymentService,
            ISettingService settingService,
            IWebHelper webHelper,
            IStoreService storeService,
            EmailAccountSettings emailAccountSettings,
            IEmailAccountService emailAccountService,
            IQueuedEmailService queuedEmailService,
            IEmailSender emailSender,
            IAddressService addressService, IDownloadService downloadService,
            OpenPayStoreSettings openPayStorePaymentSettings)
        {
            this._emailAccountSettings = emailAccountSettings;
            this._emailAccountService = emailAccountService;
            this._queuedEmailService = queuedEmailService;
            this._emailSender = emailSender;

            this._customerService = customerService;
            this._settingService = settingService;
            this._storeService = storeService;
            this._localizationService = localizationService;
            this._paymentService = paymentService;
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._openPayStorePaymentSettings = openPayStorePaymentSettings;
        }

        #endregion

        #region Methods


        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {

            string reference = string.Empty;
            string urlBarcode = string.Empty;
            string message = string.Empty;
            var result = new ProcessPaymentResult
            {
                AllowStoringCreditCardNumber = false
            };

            result.NewPaymentStatus = PaymentStatus.Authorized;
            OpenpayAPI openpayAPI;//= new OpenpayAPI(_openPayStorePaymentSettings.MerchantKey, _openPayStorePaymentSettings.MerchantId, !_openPayStorePaymentSettings.UseSandbox);

            //Put actually the openPay charge to merchant, but it can change with the follow tests

            openpayAPI = new OpenpayAPI(_openPayStorePaymentSettings.MerchantKey, _openPayStorePaymentSettings.MerchantId, !_openPayStorePaymentSettings.UseSandbox);


            Openpay.Entities.Request.ChargeRequest request = new Openpay.Entities.Request.ChargeRequest();

            request.Method = "store";
            //Deviceid and source id is not used when create payment reference
            //request.DeviceSessionId = processPaymentRequest.DeviceId;// "sah2e76qfdqa72ef2e2q";
            //request.SourceId = card.Id;


            request.Amount = new Decimal(Convert.ToDouble(processPaymentRequest.OrderTotal));
            request.Description = "Openpay con referencia de pago";

            //Validate if os necessary to add additional fee percentage, reading configuration settings


            request.OrderId = processPaymentRequest.OrderGuid.ToString();
            //request.DueDate = DateTime.Today;
            //Get customer information
            var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);
            if (customer == null)
                throw new NopException("No se pudo cargar el cliente");
            Openpay.Entities.Customer _customer = new Openpay.Entities.Customer();
            _customer.Name = customer.BillingAddress.FirstName;
            _customer.LastName = customer.BillingAddress.LastName;
            _customer.PhoneNumber = customer.BillingAddress.PhoneNumber;
            _customer.Email = customer.Email;

            //Customer information is not required to create payment reference
            request.Customer = _customer;
            Charge charge = null;
            // if (!_openPayStorePaymentSettings.UseSandbox) { 

            charge = openpayAPI.ChargeService.Create(request);
            //Generate and sned the shipment references
            //if the charge was paid succesfull, we will to generate the refence and send to the customer email
            ISession session = EngineContext.Current.Resolve<IHttpContextAccessor>().HttpContext.Session;
            ///GEt the list of packages created by pakke
            List<PakkeCoreLibrary.Model.ShipmentRequest> listShipingRequest = session.Get<List<PakkeCoreLibrary.Model.ShipmentRequest>>("PackagesInformation");
            //Get the Base url of pakke
            string baseURLPakke = session.Get<string>("PakkeBaseURL");

            //Get the token for APi pakke
            string tokenPakke = session.Get<string>("PakkeToken");

            //Get the defautl connection SMTP ion the session, and get the configuration
            var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);

            if (emailAccount == null)
                emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            if (emailAccount == null)
                throw new NopException("Email account can't be loaded");

            // Get data to create the email
            if (!string.IsNullOrEmpty(charge.PaymentMethod.Reference))
            {
                reference = charge.PaymentMethod.Reference;
                session.Set("PaymentReference", urlBarcode);
            }

            //validate the urlBarcode
            if (!string.IsNullOrEmpty(charge.PaymentMethod.BarcodeURL))
            {
                urlBarcode = charge.PaymentMethod.BarcodeURL;
            }
            else if (!string.IsNullOrEmpty(charge.PaymentMethod.BarcodeURL))
            {
                //if is null or empty, get the walmart barcode
                urlBarcode = charge.PaymentMethod.BarcodeWalmartURL;
            }
            session.Set("UrlBarCode", urlBarcode);

            //Create the body message  
            message = _openPayStorePaymentSettings.MessageOpenPay;
            message = message.Replace("#Name", _customer.Name);
            message = message.Replace("#LastName", _customer.LastName);
            message = message.Replace("#Reference", reference);
            message = message.Replace("#URLBarCode", urlBarcode);



            //Envio de tracking number al cliente
            _emailSender.SendEmail(emailAccount, _openPayStorePaymentSettings.SMTPSubjectOpenPay, message,
                    emailAccount.Email, emailAccount.DisplayName, customer.Email,
                    customer.Addresses.FirstOrDefault().FirstName + " " + customer.Addresses.FirstOrDefault().LastName, null, null, null, null, null, null);

            // }

            //Validacion de configuracion para realizar test para enviar referencias Fake

            /* if (_openPayStorePaymentSettings.TestShippingReference)
            {

                SendMessageTest();
            }
            else
            {
                //Generate the refence(s) to send to the customers by email
               foreach (var item in listShipingRequest)
                {
                    //is necessary to add trace mode to set the correct variable here 
                    PakkeCoreLibrary.Model.ShipmentResponse shipingResponse = PakkeCoreLibrary.Services.CouriersService.GenerateReference(baseURLPakke, item, tokenPakke, true);

                    string base64Binary = shipingResponse.Label;
                    byte[] bytes = Convert.FromBase64String(base64Binary);
                    //Create Download file to add the attachmentID
                    Nop.Core.Domain.Media.Download download = null;
                    download = new Core.Domain.Media.Download();
                    Guid obj = Guid.NewGuid();
                    download.DownloadBinary = bytes;
                    download.Filename = "Guia de referencia(" + shipingResponse.CourierName + ")";
                    download.Extension = ".pdf";
                    download.IsNew = true;
                    download.DownloadGuid = obj;
                    _downloadService.InsertDownload(download);
                    Nop.Core.Domain.Media.Download download2 = _downloadService.GetDownloadByGuid(obj);

                    //Envio de tracking number al cliente
                    _emailSender.SendEmail(emailAccount, "Referencia de envio Legado México", "Buen dia: " +
                        item.Recipient.Name + " , \n tu referencia de envio es: " + shipingResponse.TrackingNumber,
                        emailAccount.Email, emailAccount.DisplayName, shipingResponse.Recipient.Email,
                        shipingResponse.Recipient.Name, null, null, null, null, null, null);
                    //Envio de etiqueta de envio al vendedor
                    _emailSender.SendEmail(emailAccount, "Etiqueta de envio Legado México", "Buen dia: " +
                        item.Sender.Name + " , \n se adjunta la etiqueta de pago para el envío: " +
                        shipingResponse.TrackingNumber + "\n Cliente:  " + shipingResponse.Recipient.Name, emailAccount.Email, emailAccount.DisplayName,
                        shipingResponse.Sender.Email, shipingResponse.Sender.Name, null, null, null, null, null,
                        download2.Filename, download.Id);

                }
             }*/
            result.NewPaymentStatus = PaymentStatus.Pending;
            return result;
        }

        public void SendMessageTest()
        {
            /*  string base64Binary = "JVBERi0xLjQKJeLjz9MKMyAwIG9iago8PC9Db2xvclNwYWNlL0RldmljZVJHQi9TdWJ0eXBlL0ltYWdlL0hlaWdodCAxMi9GaWx0ZXIvRmxhdGVEZWNvZGUvVHlwZS9YT2JqZWN0L1dpZHRoIDU1L0xlbmd0aCA0ODQvQml0c1BlckNvbXBvbmVudCA4Pj5zdHJlYW0KeRYJPzsicW5R2Z8FMSPRlVNwUSbS/51JTHj6dhZIXRUr8xb3hB5qNQNaj2S7jeuN1H/IeH+4hpplDGDCRyp/jwe1dh0rJMUti/gVL51OLLGtAum6dQ9hUgXkQz6IDPeK2xMEyHZEQdWqtyDm3KEAZzJ8/ru/HE90OAqPitKoZtEd5+ZosZ+kHvz6O7TrQRFzRAdNbiNuEaNqzJsoMeA9GABFw/UAxzV4GqbBEgi/iVyEjTS2kmnqx23egWeb4v+lb2ZAKOM3eIyPCBkJxF3PVy8pojIJqNIFJzNQWj+jokEJvMZ4sZBbA9exXRRblhlBUtHlRfkrSrB+Lf2nilmQqZE6uIKQEdwbcuQXltIDIHTykqq5cYWa25NQPH6WZtkpsjZ4MIwOghjo4i//SZywPJSfInSUymO+0VZEzoWdkcneRxTDr71Ds4F4ra28xZGh4CnCJlE1cv3+UrDgUZw+MSLssaOOSgB+lktQl5pwd+s39iIjnvi47nSmMw+yPv4iHWQgIA+YaZQ9sFxMDakhSmjEOgcAmfygJvuJIPl9HovIZ2g3X8MANAUW/g5lRC2v8LyofemZ+y+RxtH3bA6tCQu5blc8zR8BPOJl/tKJ9SpUhkCBYQZnP2SANS1pIWxeHAqdfQplbmRzdHJlYW0KZW5kb2JqCjQgMCBvYmoKPDwvQ29sb3JTcGFjZS9EZXZpY2VSR0IvU3VidHlwZS9JbWFnZS9IZWlnaHQgODQvRmlsdGVyL0ZsYXRlRGVjb2RlL1R5cGUvWE9iamVjdC9EZWNvZGVQYXJtczw8L0NvbHVtbnMgMTcxL0NvbG9ycyAzL1ByZWRpY3RvciAxNS9CaXRzUGVyQ29tcG9uZW50IDg+Pi9XaWR0aCAxNzEvTGVuZ3RoIDExNDAvQml0c1BlckNvbXBvbmVudCA4Pj5zdHJlYW0KYLM3Svq3LscT+spPm8IORVCvgWnXASNq13DmwGyp7BIgnZwDmASQfnmoQPQS5wIoqS/3c7LFr8XyUlVzopgrP0Scy9V4y0PKD2EwaCEeCoHgFDVmJQOf9ZtHdnQnvTZTQlQMuQ+RGs8h9UMwjrlRWK86CXIBbZ6b3riyrGH9Ui2YnMLR/zDGfErBM/LKQkOXOL/DyfTvTi+foheWBbDMZbS7aObFywWGJcJh94rT7L3Dxw/lu2kYbH/LdbBMYTgsxTvfG2vGw9Gz6QwmvN/9yySBdyWTaPSKf584k2fYHpTABz9U/PLHSt7L5ZR4cH3KbGuSxLYqTwPBjijse3e9eu5k4COeVVF2vzBDmg8AUhOYt50ye3Jfqbz0xwQDRe7orh3NdpA4TxYL19nbafe0fY3V4ZhLMOwuksqylhpGE0y7YSJ0R78C+0HstWuagdsn08ky9vfEYHnB9R1YdGnfC/AAWQ4qi/X6wz3hNm7g9cX2ciR0gJJsRuoQ8xuwK/RknuKXqyT5mlrB096fvQTblpge3OUfL3jJ6jgCPsBn2+kdHvFdBC+fCix51+ULz+Z2juNV1W1D2qt7bRC3oa4ZPJn1knYL7NP7VAR0iBwKXoq5k4gsrCHQAp/jWY5qVAFU1/Et6R1cETJrL7178T5MSvo0me8BB6gKxiaFtT6jUcapkfMkOCJCqOpY75ubD0Zg3eLHPh1OZe+pQi1w0Gi26BJWVKBTMGgm/OXwd4q9Vrt8BN+BtNcegmhdBYbOir3VrZuP1PI9UZr6C6qHRHv2eZd/iVyxMawvpEm+DTmvx0mbvBTPjkBZKHXw9Lt6YzatzhBUEifwrBNWnbeWSrIQSwcFFHyx4PbqngXONtpNaDjB0xIXvocRStFdXk7myyWbcOOBcGUP63LAomkgKYTc6Ns8zm0kZ8mV+RLOJEvP8Y2HwyJ42wZODUp1ptKhQnJOBhC5kWK1jT61m0yMnA2obHcV9PgwRxo+agY1XJKMvpfJqjdXhiydk6k5UT8oJZbeSo5cM/t6PuoJxmi5NcwobzZNgSUxgwjqSZMZC1EKtcHnFXvWs5pJwt6JQDdZsXoHca3Pg3WejYrZcWJybFRHsr5bOz+E1Uy6416vkUNzpq1QPCy7kS6ceipnFW+43TvlwEdwemXDSUv/otY6sJsLp/KVGmWce+rtKrDrVP2HP/ej2G1H1JajXuosrtClvUX4bntWl2P9WoozhgBz9ssN+vW63nNc0H95nncjUujW7gWht8+QUxCMobt+c0A4UWIZLd4ff2gucIC+r/h8zCnVPlsx7m+QRknD3oGe1pYwihKYoDFArKtE1XlZKZUC0CfYreMR3KJqqPpnIt8WvWHftHTKGoyPKIU6SVCYIAsyzxKyIr7DugJ75ObBVguKJQL5sfNjz8JRSKxn72h/98riKe/MzB4hOnxtJkyXylpxa7NAV4/YXjhciNkFGfk40G5ED5nChfTgA8HIaOLsWfvEbIeLStYncvIts6KDvmdRGoVOFFJsCmVuZHN0cmVhbQplbmRvYmoKNSAwIG9iago8PC9Db2xvclNwYWNlL0RldmljZUdyYXkvU3VidHlwZS9JbWFnZS9IZWlnaHQgMzc0L0ZpbHRlci9GbGF0ZURlY29kZS9UeXBlL1hPYmplY3QvV2lkdGggMjkvTGVuZ3RoIDMzL0JpdHNQZXJDb21wb25lbnQgOD4+c3RyZWFtCs7iyIXZyvGOtBonh/y8hTkCRYkrKudiD7uST22PJpvgZwplbmRzdHJlYW0KZW5kb2JqCjYgMCBvYmoKPDwvQ29sb3JTcGFjZS9EZXZpY2VSR0IvU3VidHlwZS9JbWFnZS9IZWlnaHQgMzc0L0ZpbHRlci9GbGF0ZURlY29kZS9UeXBlL1hPYmplY3QvV2lkdGggMjkvU01hc2sgNSAwIFIvTGVuZ3RoIDMyMC9CaXRzUGVyQ29tcG9uZW50IDg+PnN0cmVhbQr/ncTcVbP818XRr+OuaNQCnWUNl6quktO+Z9AZ9gw/w+dMo7IPLAf1+q4WES/dFD8KWB5LkRDlb6+yCL22GNvL53osBiq8tIvjnV78N9yIV1TpaczWu2okg5O40De9x2yWy3myN9DuBnPGxxkseYcq+o8CrG8bWHRSEY4I/AS65NVThNBcpz8o1JkuTdfnRXcIaODjubDUYMQ+ZflKRG1EZfQQPaJmfRquWU3lA12uhnsEHTOi8zcwkXiJKNdxSPgj6H7Gh/tBP+CsW6BGSs+hySEn0Of8Fx8wb5XpT0m7uao6GdZIYDrug2Yr0lh6ZcL6bAx2jCtJ01siTNO91FhGH7wKj20pTucDNcEjj0Q97SdSXnY5Os7/K1rVuBvgwvHS7Dzbn/Twhge61/rXkOLLoyY38XNLQcSPQFwB4wQ+ZQplbmRzdHJlYW0KZW5kb2JqCjcgMCBvYmoKPDwvRmlsdGVyL0ZsYXRlRGVjb2RlL0xlbmd0aCAxMjQzPj5zdHJlYW0KkwvMt3QT4pEjxGIvQnE7SFY0Jo3iXqrYxwuA5poPsfQ5pFNJt7AHgJ/hFDGdedBeueVd/wAE0son8wXgNieWWhmki5l1afVB2pr69GrI3UGksLyWq3wJ3Yj6NGWiEUUulCrw96u/9mOdYb96Od4kUQrec6E2jZVZAdPsupg1P/SJ27TjpAvH8/GiHhP1M1YqmoU7Ajl5EiquWLhSvF30moJVuorMgskJJbtBk4dC1oUsnOBZsLYHGSSTSdeMrq2Ei/3RqehReqlFvo55a9K1SrYyPsLVTUuCGEV7494WwEdiFzU20YDh3pdWkX5yr0C7HN0d39xOn3LS/KhkIKF+8k8OA84+2S+pGv1jZYKN3wcV9a4RuMUSIYGtDsf6vCZrFNqF+XaMY2h9G2yVMTJFnbg1ezD0nK9TblSHZBtR4QA32kxj/B3MKUKFOpPQ4GfIcJowxzRTtwIhYXfKmdUUMUTDU910mffR/5NV5Xfshr3PvYEgyi7iU1snrD8aYUvXYcXC7rREqXWJ29lVckuty2jdEi7P++/7e6pg/S4/xCxnyrizEvYo4aQYoHwqIfwxP23ekPmoS2XLp6d7Zf2ThSYD9iLlUsKIJYDRxOiFvZqIKet3XeSqEljGQi7JFuyqqCXb+Ki/K9GKW0Ln6FPgJcccWjqadzHqQtONGFM5OwXXsatXjnSchisBZZ9wa4Q2k1nk2hhkaemLnL6Ft8XyGYbM5Yj/REV71ca7tH4wQaB5eUyg9cE3UsVt083J+Ry+7gJzKN6fLtJvqe4gTw3X73F9yFVHMsNiZFbBbePJimzwuBdrVLaKbI68tbADpy7jf6iJFb98ZEDCzMExwlDNAVeDNBiMLEYk0i6+rTLkR5Eehu2FT4lHKGstxzSK43g8wNvwaYZkKUtUbL+NOURmUknIuKKIB/HmtTHAguePrhdq3NhwjATmzXhx8QJ1G19j0W1SvDLKbQLBFP7Hd3NgqB5NAwH85DpY0XebjVrLOGjySct2BAhgxoxBESXnIFxW9Au2R1a+2N78e8XSwEhpdHHKNYXakGto5NtL2bJrqYAwAhtBRmmdR2vWfkFK0F80vOVBdGvziKMkI8Ga8nzUuHMaq/uPt+NYpaMONGfKj2vfTmp7HL1IFWwdyaoOB6ABKygA2nl70xLKcE0k2d+QnvRKyKUydy6j5g2HnGl6iH57pmS59sMn2a4uJ/YoEtGJ8/nE0sG27dpHeCvFUJLJWBEoL8LU1YP8ChVcIJQW5/uD1VNazFiLF6AoJgtJO/YjwhXnfGKT7ai30tE3ARteRHD4lRCnml5KwtnXQUfD/tQQnM+WHJOb7jWUJFuUQ3Q85FV8TIeEa/VatwbpPN7uSKqSNWewNx5hs1RPrkOsA2EHmUv6nqqahx/AROapw9TI6RjFt+JxRhADA/l9PhQqXGIovDSy2ekeN4Fc5CxUDGQUPyfh5aEy8dtnGIJXErpLAvavUECG9JOjkbiPxwJEeD3xtS9GzajZHi+r8au9gFu8RK0Y0TQZHv3Ooy9yqd9hdAMHm2oG4k0NmQ+pDdKP3x9q4R7LsjA+VRvuchVZNMSqFOE2wS2qXV9eWVxW9ycQC6zDsv1FVwSSU5bDu+1btOuCJWZk2tN79qq/X2DYKoqvImspjbIFNdGxOgplbmRzdHJlYW0KZW5kb2JqCjEgMCBvYmoKPDwvR3JvdXA8PC9TL1RyYW5zcGFyZW5jeS9UeXBlL0dyb3VwL0NTL0RldmljZVJHQj4+L0NvbnRlbnRzIDcgMCBSL1R5cGUvUGFnZS9SZXNvdXJjZXM8PC9Db2xvclNwYWNlPDwvQ1MvRGV2aWNlUkdCPj4vUHJvY1NldCBbL1BERiAvVGV4dCAvSW1hZ2VCIC9JbWFnZUMgL0ltYWdlSV0vRm9udDw8L0YxIDIgMCBSPj4vWE9iamVjdDw8L2ltZzMgNiAwIFIvaW1nMiA1IDAgUi9pbWcxIDQgMCBSL2ltZzAgMyAwIFI+Pj4+L1BhcmVudCA4IDAgUi9NZWRpYUJveFswIDAgMjg4IDQzMl0+PgplbmRvYmoKOSAwIG9iagpbMSAwIFIvWFlaIDAgNDQyIDBdCmVuZG9iagoyIDAgb2JqCjw8L1N1YnR5cGUvVHlwZTEvVHlwZS9Gb250L0Jhc2VGb250L0hlbHZldGljYS9FbmNvZGluZy9XaW5BbnNpRW5jb2Rpbmc+PgplbmRvYmoKOCAwIG9iago8PC9LaWRzWzEgMCBSXS9UeXBlL1BhZ2VzL0NvdW50IDEvSVRYVCj529KBxyk+PgplbmRvYmoKMTAgMCBvYmoKPDwvTmFtZXNbKPfUvdBcXHDylWRBXFwEb25O6nuGKSA5IDAgUl0+PgplbmRvYmoKMTEgMCBvYmoKPDwvRGVzdHMgMTAgMCBSPj4KZW5kb2JqCjEyIDAgb2JqCjw8L05hbWVzIDExIDAgUi9UeXBlL0NhdGFsb2cvUGFnZXMgOCAwIFIvVmlld2VyUHJlZmVyZW5jZXM8PC9QcmludFNjYWxpbmcvQXBwRGVmYXVsdD4+Pj4KZW5kb2JqCjEzIDAgb2JqCjw8L01vZERhdGUo4mCZ7HbMeQv13JS0WfYvse7aVxf4y7ApL0NyZWF0b3Io7DvYrCKHG1a0itfyGeU15aKZFlW6qfJcXBXydIPdyMmt387hKS9DcmVhdGlvbkRhdGUo4mCZ7HbMeQv13JS0WfYvse7aVxf4y7ApL1Byb2R1Y2VyKM8OzqQz1Xsd9cuSplxivD25l9k6ZCk+PgplbmRvYmoKMTQgMCBvYmoKPDwvTyAoy1wpR5elut4lEKN10FwpN45SBHPCXG50zQsbrER+MrtaWO8pL1AgLTEzNDAvUiAzL0ZpbHRlci9TdGFuZGFyZC9VICgdGjmHgi4Wg9Yx1zaaP1xmpgAAAAAAAAAAAAAAAAAAAAApL1YgMi9MZW5ndGggMTI4Pj4KZW5kb2JqCnhyZWYKMCAxNQowMDAwMDAwMDAwIDY1NTM1IGYgCjAwMDAwMDQwMDAgMDAwMDAgbiAKMDAwMDAwNDMyNSAwMDAwMCBuIAowMDAwMDAwMDE1IDAwMDAwIG4gCjAwMDAwMDA2NTIgMDAwMDAgbiAKMDAwMDAwMjAxNiAwMDAwMCBuIAowMDAwMDAyMjAzIDAwMDAwIG4gCjAwMDAwMDI2ODkgMDAwMDAgbiAKMDAwMDAwNDQxMyAwMDAwMCBuIAowMDAwMDA0MjkwIDAwMDAwIG4gCjAwMDAwMDQ0NzYgMDAwMDAgbiAKMDAwMDAwNDUzMyAwMDAwMCBuIAowMDAwMDA0NTY3IDAwMDAwIG4gCjAwMDAwMDQ2NzIgMDAwMDAgbiAKMDAwMDAwNDg0MiAwMDAwMCBuIAp0cmFpbGVyCjw8L0luZm8gMTMgMCBSL0VuY3J5cHQgMTQgMCBSL0lEIFs8NjIyOGFiNzVjNGQyNjQzYjBkNjJkYWU2OTM3MWEyYTQ+PGUwYzcxMjM1NDcwYjNmZjAwMTUzNmI5NzJhODM5MjY4Pl0vUm9vdCAxMiAwIFIvU2l6ZSAxNT4+CnN0YXJ0eHJlZgo0OTg0CiUlRU9GCg==";
              byte[] bytes = Convert.FromBase64String(base64Binary);
              //Create Download file to add the attachmentID
              Nop.Core.Domain.Media.Download download = null;
              download = new Core.Domain.Media.Download();
              Guid obj = Guid.NewGuid();
              download.DownloadBinary = bytes;
              download.Filename = "Test Reference";
              download.Extension = ".pdf";
              download.IsNew = true;
              download.DownloadGuid = obj;
              _downloadService.InsertDownload(download);
              Nop.Core.Domain.Media.Download download2 = _downloadService.GetDownloadByGuid(obj);
              var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);

              if (emailAccount == null)
                  emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
              if (emailAccount == null)
                  throw new NopException("Email account can't be loaded");
              _emailSender.SendEmail(emailAccount, "Etiqueta creada por Legado Mexico", "Prueba de envio de referencia de envio", emailAccount.Email, emailAccount.DisplayName, _openPayStorePaymentSettings.VendorEmailFake, "Test", null, null, null, null, null, download2.Filename, download.Id);
              _emailSender.SendEmail(emailAccount, "Referencia creada por Legado Mexico", "Prueba de envio de referencia de envio, tu referencia es 12345647", emailAccount.Email, emailAccount.DisplayName, _openPayStorePaymentSettings.CustomerEmailFake, "Test", null, null, null, null, null, null);
              */
        }
        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/PaymentOpenPayStore/Configure";
        }

        public string GetPublicViewComponentName()
        {
            return "PaymentOpenPayStore";
        }

        /// <summary>
        /// Install the plugin
        /// </summary>
        public override void Install()
        {
            //settings
            _settingService.SaveSetting(new OpenPayStoreSettings
            {
                UseSandbox = true,
                //TestShippingReference = true
            });

            //locales
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.AdditionalFee", "Cuota Adicional");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.AdditionalFee.Hint", "Introduzca una tarifa adicional para cobrar a su clientes.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.AdditionalFeePercentage", "Cuota Adicional. Porcentaje de uso");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.AdditionalFeePercentage.Hint", "Determina si se aplica una tarifa adicional porcentual al total del pedido. Si no está habilitado, se usa un valor fijo.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MerchantId", "Merchant ID");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MerchantId.Hint", "Especificar ID de comerciante.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MerchantKey", "Llave privada OpenPay");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MerchantKey.Hint", "Especificar llave privade de comerciante.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.UseSandbox", "Usar Modo Prueba OpenPay");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.UseSandbox.Hint", "Marque para habilitar Sandbox (entorno de prueba).");

            //_localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.TestShippingReference", "Prueba de creado de referencia");
            //_localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.TestShippingReference.Hint", "Al activarse esta casilla , se habilita el modo prueba de generacion y envio de referencia Hard Code. DEben de Agregar mandatorios los campos siguientes de correos de prueba");
            //_localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.CustomerEmailFake", "Email de cliente Prueba");
            //_localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.CustomerEmailFake.Hint", "Email de cliente que se usara para mandar correos de prueba.");
            //_localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.VendorEmailFake", "Email de vendedor de prueba");
            //_localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.VendorEmailFake.Hint", "Email de cliente que se usara para mandar correos de prueba.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.SMTPSubjectOpenPay", "Asunto OpenPay");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.SMTPSubjectOpenPay.Hint", "Asunto que aparecerá en los correos que se envien de Open pay.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MessageOpenPay", "Mensaje OpenPay");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MessageOpenPay.Hint", "Mensaje que se enviará en cada correo en Open pay.");
            //_localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.SMTPSubjectPakke", "Asunto Pakke");
            //_localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.SMTPSubjectPakke.Hint", "Asunto que aparecerá en los correos que se envien de open pay.");
            //_localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MessagePakke", "Mensaje Pakke");
            //_localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MessagePakke.Hint", "Mensaje que se enviará en cada correo en pakke.");

            //_localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.RedirectionTip", "Será redirigido al sitio de OpenPayStore para completar el pedido.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.OpenPayStore.PaymentMethodDescription", "Referencía de pago para tiendas afiliadas.");

            base.Install();
        }

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<OpenPayStoreSettings>();

            //locales
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.AdditionalFee");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.AdditionalFee.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.AdditionalFeePercentage");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.AdditionalFeePercentage.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MerchantId");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MerchantId.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MerchantKey");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MerchantKey.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.UseSandbox");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.UseSandbox.Hint");

            //_localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.TestShippingReference");
            //_localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.TestShippingReference.Hint");
            //_localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.CustomerEmailFake");
            //_localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.CustomerEmailFake.Hint");
            //_localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.VendorEmailFake");
            //_localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.VendorEmailFake.Hint");

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.RedirectionTip");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.PaymentMethodDescription");

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.SMTPSubjectOpenPay");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MessageOpenPay");
            //_localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.SMTPSubjectPakke");
            //_localizationService.DeletePluginLocaleResource("Plugins.Payments.OpenPayStore.Fields.MessagePakke");
            base.Uninstall();
        }

        public bool SupportCapture
        {
            get { return true; }
        }
        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get { return true; }
        }
        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get { return RecurringPaymentType.Manual; }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public Nop.Services.Payments.PaymentMethodType PaymentMethodType
        {
            get { return Nop.Services.Payments.PaymentMethodType.Standard; }
        }

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo
        {
            get { return false; }
        }

        public string PaymentMethodDescription
        {
            get { return _localizationService.GetResource("Plugins.Payments.OpenPayStore.PaymentMethodDescription"); }
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            //always success
            var result = new CancelRecurringPaymentResult();
            result.AddError("Pago recurrente no soportado");
            return result;
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            return order.OrderStatus == OrderStatus.Pending;
        }
        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            result.AddError("Método de captura no soportado");
            return result;
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            return _paymentService.CalculateAdditionalFee(cart,
                _openPayStorePaymentSettings.AdditionalFee, _openPayStorePaymentSettings.AdditionalFeePercentage);
        }

        public ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {
            return new ProcessPaymentRequest
            {
                //DeviceId = form["DeviceId"]
            };
        }

        /// <summary>
        /// Returns a value indicating whether payment method should be hidden during checkout
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>true - hide; false - display.</returns>
        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            //you can put any logic here
            //for example, hide this payment method if all products in the cart are downloadable
            //or hide this payment method if current customer is from certain country
            return false;
        }
        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {

        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.AddError("Pago recurrente no soportado");
            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();
            result.AddError("Refund method not supported");
            return result;
        }

        public IList<string> ValidatePaymentForm(IFormCollection form)
        {
            return new List<string>();
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();
            result.AddError("Void method not supported");
            return result;
        }
        #endregion

        #region Utilities
        /// <summary>
        /// Process a regular or recurring payment
        /// </summary>
        /// <param name="paymentRequest">Payment request parameters</param>
        /// <param name="isRecurringPayment">Whether it is a recurring payment</param>
        /// <returns>Process payment result</returns>
        private ProcessPaymentResult ProcessPayment(ProcessPaymentRequest paymentRequest, bool isRecurringPayment)
        {
            //var pago = new Openpay.OpenpayAPI(_openPayStorePaymentSettings.MerchantKey, _openPayStorePaymentSettings.MerchantId, false);
            //return result
            var result = new ProcessPaymentResult { AllowStoringCreditCardNumber = true };
            return result;
        }
        #endregion

    }
}
