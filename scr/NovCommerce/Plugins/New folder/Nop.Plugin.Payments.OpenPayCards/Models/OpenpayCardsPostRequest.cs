﻿using Newtonsoft.Json;
using Nop.Plugin.Payments.OpenPayCards.Domain.Models;
using Nop.Plugin.Payments.OpenPayCards.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.OpenPayCards.Models
{
    public abstract class OpenpayCardsPostRequest: OpenPayCardsRequest
    {
        /// <summary>
        /// Gets or sets a developer Id and version information related to the integration.
        /// </summary>
        [JsonProperty("developerApplication")]
        public DeveloperApplication DeveloperApplication { get; set; }
    }
}
