﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.OpenPayCards.Domain.Enums
{
    public class NullableStringEnumConverter:StringEnumConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            try
            {
                //try to read JSON as usual
                return base.ReadJson(reader, objectType, existingValue, serializer);
            }
            catch (JsonSerializationException exception)
            {
                //if object type is nullable enum, set unknown value as null
                if (Nullable.GetUnderlyingType(objectType)?.IsEnum ?? false)
                    return null;

                throw exception;
            }
        }
    }
}
