﻿using Nop.Core.Configuration;
using Nop.Plugin.Payments.OpenPayCards.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.OpenPayCards
{
    public class OpenPayCardsSettings : ISettings
    {
        /// <summary>
        /// Gets or sets merchant ID
        /// </summary>
        public string MerchantId { get; set; }

        /// <summary>
        /// Gets or sets merchant key
        /// </summary>
        public string PublicApiKey { get; set; }

        public string PrivateApiKey { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether to use sandbox (testing environment)
        /// </summary>
        public bool UseSandbox { get; set; }
        /// <summary>
        /// This value enable the test to send reference message
        /// </summary>
        public bool TestShippingReference { get; set; }
        /// <summary>
        /// Email fake to  send the reference
        /// </summary>
        public string VendorEmailFake { get; set; }
        public string CustomerEmailFake { get; set; }
        /// <summary>
        /// Gets or sets an additional fee
        /// </summary>
        public decimal AdditionalFee { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to "additional fee" is specified as percentage. true - percentage, false - fixed value.
        /// </summary>
        public bool AdditionalFeePercentage { get; set; }

 
 
        public string SMTPSubjectPakke { get; set; }

     

        public string MessagePakke { get; set; }
    }
}
