﻿using Nop.Core.Infrastructure;
using Nop.Services.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.OpenPayCards
{
    public class OpenPayCardsPaymentDefaults
    {
        /// <summary>
        /// OpenPay store payment method system name
        /// </summary>
        public static string SystemName => "Payments.OpenPayCards";

        /// <summary>
        /// Name of the view component to display plugin in public store
        /// </summary>
        public const string ViewComponentName = "PaymentOpenPayCards";

        /// <summary>
        /// User agent used for requesting Worldpay services
        /// </summary>
        public static string UserAgent => "nopCommerce-plugin-3.0";

        /// <summary>
        /// Path to the openPay payment js script
        /// </summary>
        public static string PaymentScriptPathLegadoMexico => "http://akramire-001-site1.ctempurl.com/js/LegadoMexico.js";

        /// <summary>
        /// Path to the openPay payment js script 2
        /// </summary>
        public static string PaymentScriptPathOpenPay2 => "https://openpay.s3.amazonaws.com/openpay.v1.js";

        public static string PaymentScriptPathOpenPay3 => "https://openpay.s3.amazonaws.com/openpay-data.v1.js";

        /// <summary>
        /// Path to the jquery script
        /// </summary>
        public static string PaymentScriptPathJquery => "https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js";


        /// <summary>
        /// Key of the attribute to store Worldpay Vault customer identifier
        /// </summary>
        public static string CustomerIdAttribute => "OpenPayCardsCustomerId";

        /// <summary>
        /// Certified nopCommerce developer application ID
        /// </summary>
        public static string DeveloperId => "10000786";

        /// <summary>
        /// Certified nopCommerce developer application version
        /// </summary>
        public static string DeveloperVersion => EngineContext.Current.Resolve<IPaymentService>()?
            .LoadPaymentMethodBySystemName(SystemName)?.PluginDescriptor?.Version ?? "3.10";

        /// <summary>
        /// Sandbox application ID
        /// </summary>
        public static string SandboxDeveloperId => "12345678";

        /// <summary>
        /// Sandbox application version
        /// </summary>
        public static string SandboxDeveloperVersion => "1.2";
    }
}
