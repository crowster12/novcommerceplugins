﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Vendors;
using System;
using System.Collections.Generic;
using System.Text;

namespace PakkeCoreLibrary.Model
{
    public  class PackagePakke
    {
        #region Properties      
        public List<PackagePakke> listPackages { get; set; }
        public ParcelEntity parcel { get; set; }
        //Informacion de origen y destino 
        public Rate rate { get; set; }
        //Ide del vendedor
         public Vendor vendor { get; set; }

         public Customer customer { get; set; }

        public PakkeEntity pakkeEntity { get; set; }

        //Numero de paquetes creados para este cliente
        public int totalPackages { get; set; }
        //Numero de paquete del total
        public int noPackage { get; set; }

        //public PackingType packingType { get; set; }

        public string Description { get; set; }
        #endregion
    }
}
