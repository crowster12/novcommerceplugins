﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PakkeCoreLibrary.Model
{
    public class ParcelEntity
    {
        /// <summary>
        /// Longitud, largo
        /// </summary>
        public double Length { get; set; }
        /// <summary>
        /// Anchura
        /// </summary>
        public double Width { get; set; }
        /// <summary>
        /// Altura
        /// </summary>
        public double Height { get; set; }
        /// <summary>
        /// Peso
        /// </summary>
        public double Weight { get; set; }
     //   public double VolumetricWeight { get; set; }
    }
}
