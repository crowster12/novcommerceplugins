﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PakkeCoreLibrary.Model
{
    public class AddressFromEntity
    {
        #region Properties
        public string ZipCode { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Neighborhood { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public bool Residential { get; set; }
        #endregion
    }
}
