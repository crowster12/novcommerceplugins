﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PakkeCoreLibrary.Model
{
    public class ShipmentResponse
    {
        public string ShipmentId { get; set; }
     
        public string  ResellerId { get; set; }
        public string OwnerId { get; set; }
        public string OrderId { get; set; }

        public string CreatedAt { get; set; }
        public string ExpiresAt { get; set; }
        public string CourierName { get; set; }
        public string CourierCode { get; set; }
        public string CourierServiceId { get; set; }
        public string CourierService { get; set; }
        public string ResellerReference { get; set; }

        public bool HasExceptions { get; set; }
        public bool HasChangeZipCode { get; set; }
        public bool SendRecipientNotifications { get; set; }

        public int InsuredAmount { get; set; }

        public PakkeEntity Parcel { get; set; }

        public double QuotedWeight { get; set; }
        public double RealWeight { get; set; }
        public double RealOverWeight { get; set; }
        public double CoveredWeight { get; set; }
        public double OverWeight { get; set; }
        public double OverWeightPrice { get; set; }
        public double CoveredAmount { get; set; }
        public double ExtraAmount { get; set; }
        public double QuotedAmount { get; set; }
        public double DiscountAmount { get; set; }
        public string CouponCode { get; set; }
        public double InsuranceAmount { get; set; }
        public double InsurancePercentFactor { get; set; }
        public double TotalAmount { get; set; }
        public double OriginalWeigth  { get; set; }
        public double OriginalWidth { get; set; }
        public double OriginalLength { get; set; }
        public double OriginalHeight { get; set; }
        public double OriginalVolumetricWeight { get; set; }
        public AddressFromEntity AdrressFrom { get; set; }
        public AddressToEntity AdrressTo { get; set; }
        public SenderEntity Sender { get; set; }
        public RecipientEntity Recipient { get; set; }
 
      //  public string ServiceTypeCode { get; set; }

        public string Owner { get; set; }
        public string DaysInTransit { get; set; }
        public int EnableRefund { get; set; }
        public string ChangeZipCode { get; set; }
        public string Content { get; set; }
        //public Object Transactions { get; set; }
        // public string Credentials { get; set; }

        public string TrackingNumberReplaced { get; set; }
        public string Folio { get; set; }
        public string EstimatedDeliveryDate { get; set; }
        public string TrackingNumber { get; set; }
        public string WaybillNumber { get; set; }
        public string Label { get; set; }
        public string Status { get; set; }
        public string TrackingStatus { get; set; }







    }
}
