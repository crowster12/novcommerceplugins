﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PakkeCoreLibrary.Model
{
    public class Rate
    {
        public ParcelEntity parcel { get; set; }
        public string ZipCodeFrom { get; set; }
        public string ZipCodeTo { get; set; }
    }
}
