﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Services.Logging;
using PakkeCoreLibrary.Model;
using PakkeCoreLibrary.Tool;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PakkeCoreLibrary.Services
{
    public class CouriersService
    {
         
        /// <summary>
        /// Get the list of Couriers
        /// </summary>
        /// <param name="BASE_URL">URL where the rest services are hosted</param>
        /// <param name="accessToken">Access token of the pakke api</param>
        /// <param name="Trace">Flag to enable trace of code</param>
        /// 
        /// 
        /// <returns></returns>
        public static CourierEntity Get(string BASE_URL,  string accessToken, bool Trace)
        {
            CourierEntity result = new CourierEntity();
            string json = string.Empty;
            StringBuilder routePath = new StringBuilder();
            routePath.Append("/");
            routePath.Append("CourierServices/");
 
            try
            {
                json = Tool.WsUtil.ExecuteGetRestService(Trace, BASE_URL, routePath.ToString(),accessToken);
                if (string.IsNullOrEmpty(json))
                {
                    throw new Exception("Json is empty or null");
                }
                result.ListCouriers = JsonConvert.DeserializeObject<List<CourierEntity>>(json);
            }
            catch (Exception we)
            {
                EventLogEntry exception = new EventLogEntry();
                try
                {
                    exception = JsonConvert.DeserializeObject<EventLogEntry>(we.Message);
                    throw new Exception("Error: " + exception.code + "\n " + exception.details);
                }
                catch (Exception)
                {
                    throw;
                }
                throw;
            }
            return result;
        }
        /// <summary>
        /// GEt a list of couriers with rate information
        /// </summary>
        /// <param name="BASE_URL">Base url of the api</param>
        /// <param name="rate"></param>
        /// <param name="accessToken"></param>
        /// <param name="Trace"></param>
        /// <returns></returns>
        public static PakkeEntity GetRates(string BASE_URL, Rate rate, string accessToken, bool Trace) {

            PakkeEntity result = new PakkeEntity();
            string entityJson = string.Empty;
            string json = string.Empty;
            StringBuilder routePath = new StringBuilder();
            routePath.Append("/");
            routePath.Append("Shipments/");
            routePath.Append("rates/");
            try
            {
                 entityJson=JsonConvert.SerializeObject(rate);
                entityJson= entityJson.Replace("\\", string.Empty);
                entityJson = entityJson.Replace("parcel", "Parcel");
                json = Tool.WsUtil.ExecutePostRestService(Trace, BASE_URL, routePath.ToString(), entityJson, accessToken);
                if (string.IsNullOrEmpty(json))
                {
                    throw new Exception("Json is empty or null");
                }
                result = JsonConvert.DeserializeObject<PakkeEntity>(json);

            }
            catch (Exception ex)
            {

                throw;
            }
            return result;
        }

        /// <summary>
        /// Method to generate the de reference
        /// </summary>
        /// <param name="BASE_URL">Base url of the api</param>
        /// <param name="shipment">Shipment request</param>
        /// <param name="accessToken">Access token to access to the api</param>
        /// <param name="Trace">Enable or disable debug</param>
        /// <returns></returns>
        public static ShipmentResponse GenerateReference(string BASE_URL, ShipmentRequest shipmentRequest, string accessToken, bool Trace)
        {
         
            ShipmentResponse result = new ShipmentResponse();
            string entityJson = string.Empty;
            string json = string.Empty;
            StringBuilder routePath = new StringBuilder();
            routePath.Append("/");
            routePath.Append("Shipments/");
            try
            {
                entityJson = JsonConvert.SerializeObject(shipmentRequest);
                json = WsUtil.ExecutePostRestService(Trace, BASE_URL, routePath.ToString(), entityJson, accessToken);
                if (string.IsNullOrEmpty(json))
                {
                    throw new Exception("Json is empty or null");
                }
                result = JsonConvert.DeserializeObject<ShipmentResponse>(json);

            }
            catch (Exception ex)
            {

                throw;
            }
            return result;
        }
    }
}
