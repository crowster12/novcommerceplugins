﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PakkeLibrary.Model
{
    public class EventLogEntry
    {

        public int EventLogId { get; }

        public EventLogEntryType EventLogType { get; }
        public string EventSource { get; }
        public string Description { get; set; }

        //public string IPAddress { get; } i commented this property beacuse I dont use rigth now, but in a future qe can use it

    }
    /// <summary>
    /// Enum for event Log Entry types
    /// </summary>
    public enum EventLogEntryType : byte
    {
        Information = 0,
        Warning = 1,
        Error = 2
    }
}
