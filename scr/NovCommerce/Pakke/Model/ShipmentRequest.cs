﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PakkeLibrary.Model
{
    public class ShipmentRequest
    {
        #region Properties
        public string CourierCode { get; set; }
        public string CourierServiceId { get; set; }
        public string ResellerReference { get; set; }
        public AddressFromEntity AddressFrom { get; set; }
        public AddressToEntity AddressTo { get; set; }
        public ParcelEntity Parcel { get; set; }
        public SenderEntity Sender { get; set; }
        public RecipientEntity Recipient { get; set; }
        public string Content { get; set; }
        #endregion
    }
 
}
