﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PakkeLibrary.Model
{
    public class PackagePakkeEntity
    {
        public ParcelEntity parcel { get; set; }
        //Informacion de origen y destino 
        public Rate rate { get; set; }
        //Ide del vendedor
        // public Vendor vendor { get; set; }

        // public Customer customer { get; set; }

        public PakkeEntity pakkeEntity { get; set; }

        //Numero de paquetes creados para este cliente
        public int totalPackages { get; set; }
        //Numero de paquete del total
        public int noPackage { get; set; }

        // public PackingType packingType { get; set; }

        public string Description { get; set; }
    }
}
