﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PakkeLibrary.Model
{
    /// <summary>
    /// Class to storage the courier information
    /// </summary>
    public class CourierEntity
    {
        public string CourierServiceId { get; set; }
        public string CourierCode { get; set; }
        public string Description { get; set; }
        public string DeliveryDays { get; set; }

        public List<CourierEntity> ListCouriers{ get; set; }
    }
}
