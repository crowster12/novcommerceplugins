﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PakkeLibrary.Model
{
    public class RecipientEntity
    {

        #region Properties
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string Phone1 { get; set; }
        public string Email { get; set; }
        #endregion
    }
}
