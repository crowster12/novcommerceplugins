﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PakkeLibrary.Model
{
    public class PakkeEntity
    {
        public string CourierCode { get; set; }
        public string CourierName { get; set; }
        public string CourierServiceId { get; set; }
        public string CourierServiceName { get; set; }
        public string DeliveryDays { get; set; }
        public string CouponCode { get; set; }

        public int DiscountAmount { get; set; }
        public double TotalPrice { get; set; }
        public string EstimatedDeliveryDate { get; set; }


        public bool BestOption { get; set; }

        public List<PakkeEntity> Pakke { get; set; }
    }
}
 