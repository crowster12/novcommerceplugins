﻿
using Newtonsoft.Json;
using PakkeLibrary.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace PakkeLibrary.Tool
{
    public class WsUtil
    {
        static bool onlyTraceErrors = false;
        static string json = string.Empty;


        //static variable to create an instance of log4Net to log what is happening when call the RestServices
        //private static readonly log4net.ILog log =
        // log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        /// <summary>
        /// Static method to execute REST Service 'GET', and returns a json result if the status is OK, in another hand returns an exception
        /// </summary>
        /// <param name="trace">If we want to trace the put true, or in other hand false</param>
        /// <param name="base_URI">Url base where the services are hosted</param>
        /// <param name="path">Path of the specific Rest Service to execute, this will concat to the base_URI</param>
        /// <returns>json</returns>
        public static string ExecuteGetRestService(bool trace, string base_URI, string path, string accessToken)
        {
            string resultado = string.Empty;
            try
            {
                ServicePointManager.ServerCertificateValidationCallback =
                    delegate (Object obj, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors errors)
                    {
                        return true;
                    };
                //WebRequest request = WebRequest.Create(base_URI + System.Web.HttpUtility.UrlPathEncode(path));
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create((base_URI + System.Web.HttpUtility.UrlPathEncode(path)));
                request.Accept = "*/*";
                request.AllowAutoRedirect = true;
                request.UserAgent = "http_requester/0.1";
                request.Timeout = 60000;
                request.Method = "GET";
                request.Headers.Add("Authorization",     accessToken);
                request.ContentType = "application/json";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream(),
                                      ASCIIEncoding.UTF8);
                json = reader.ReadToEnd();
                reader.Close();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    resultado = json;

                    if (resultado.Contains("EventLog"))
                        throw new Exception(resultado);
                    if (trace && !onlyTraceErrors) { }
                       // log.Info("\n*\n* [GET] Resultado al ejecutar el servicio web==>: \n* "
                        //    + base_URI + path + "\n* [RESULTADO] \n*" + resultado + "\n* [FIN RESULTADO] \n*\n*");
                }
                else
                {
                    throw new Exception(resultado);
                }
            }
            catch (WebException we)
            {

                if (trace)
                {
                   /* log.Error("\n* [GET] Error al ejecutar el servicio web() ==>: \n*" + base_URI + path);
                    log.Error("\n* [GET] Descripción del error ==>: " + we);
                    log.Error("\n* [GET] Método ejecutado ==>: " + System.Reflection.MethodBase.GetCurrentMethod().Name);*/
                }

                StreamReader reader = new StreamReader(we.Response.GetResponseStream(),
                ASCIIEncoding.UTF8);
                json = reader.ReadToEnd();
                reader.Close();

                if (!string.IsNullOrEmpty(json))
                {
                    try
                    {
                        EventLogEntry exception = new EventLogEntry();
                        exception = JsonConvert.DeserializeObject<EventLogEntry>(json);
                        //If exist an exception object, we will return as a string
                        if (exception != null)
                            //return exception.Description.ToString();
                            throw new Exception(exception.Description.ToString());
                        else
                        {
                            throw we;
                        }
                    }
                    catch (Exception ex)
                    {

                        throw new Exception(ex.ToString()); ;
                    }
                }
            }
            return resultado;

        }

        /// <summary>
        /// Staticmethod to execute Restservice 'POST'
        /// </summary>
        /// <param name="trace">If we want to trace the put true, or in other hand false</param>
        /// <param name="base_URI"></param>
        /// <param name="path">Path of the specific Rest Service to execute, this will concat to the base_URI</param>
        /// <returns></returns>
        public static string ExecutePostRestService(bool trace, string base_URI, string path, string entityJson, string accessToken)
        {
            string resultado = string.Empty;
            try
            {
                /*ServicePointManager.ServerCertificateValidationCallback =
                    delegate (Object obj, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors errors)
                    {
                        return true;
                    };*/

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create((base_URI + System.Web.HttpUtility.UrlPathEncode(path)));
                var postData = entityJson;
                var data = Encoding.UTF8.GetBytes(postData);
                request.Accept = "*/*";
                request.AllowAutoRedirect = true;
                request.UserAgent = "http_requester/0.1";
                request.Timeout = 60000;
                request.Method = "POST";
                //request.Headers.Add("Content-transfer-encoding", "application/json");
                
                request.ContentType = "application/json";
                request.Headers.Add("Authorization", accessToken);

         
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(response.GetResponseStream(),
                                      ASCIIEncoding.UTF8);
                json = reader.ReadToEnd();
                reader.Close();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    resultado = json;
                    if (resultado.Contains("EventLog"))
                        throw new Exception(resultado);
                    if (trace && !onlyTraceErrors) { }

                      /*  log.Info("\n*\n* [GET] Resultado al ejecutar el servicio web==>: \n* "
                            + base_URI + path + "\n* [RESULTADO] \n*" + resultado + "\n* [FIN RESULTADO] \n*\n*");*/
                }
                else
                {
                    throw new Exception(resultado);
                }
            }
            catch (WebException we)
            {

                if (trace)
                {
                   /* log.Error("\n* [GET] Error al ejecutar el servicio web() ==>: \n*" + base_URI + path);
                    log.Error("\n* [GET] Descripción del error ==>: " + we);
                    log.Error("\n* [GET] Método ejecutado ==>: " + System.Reflection.MethodBase.GetCurrentMethod().Name);*/
                }

                StreamReader reader = new StreamReader(we.Response.GetResponseStream(),
                ASCIIEncoding.UTF8);
                json = reader.ReadToEnd();
                reader.Close();

                if (!string.IsNullOrEmpty(json))
                {
                    try
                    {
                        EventLogEntry exception = new EventLogEntry();
                        exception = JsonConvert.DeserializeObject<EventLogEntry>(json);
                        //If exist an exception object, we will return as a string
                        if (exception != null)
                            //return exception.Description.ToString();
                            throw new Exception(exception.Description.ToString());
                        else
                        {
                            throw we;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.ToString()); ;
                    }
                }
            }
            return resultado;
        }
        /// <summary>
        /// Static method to execute Restservice 'DELETE'
        /// </summary>
        /// <param name="trace">If we want to trace the put true, or in other hand false</param>
        /// <param name="base_URI"></param>
        /// <param name="path">Path of the specific Rest Service to execute, this will concat to the base_URI</param>
        /// <returns></returns>
        public static string ExecuteDeleteRestService(bool trace, string base_URI, string path, string entityJson, string accessToken)
        {
            string resultado = string.Empty;
            try
            {
                ServicePointManager.ServerCertificateValidationCallback =
                    delegate (Object obj, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors errors)
                    {
                        return true;
                    };
                //WebRequest request = WebRequest.Create(base_URI + System.Web.HttpUtility.UrlPathEncode(path));
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create((base_URI + System.Web.HttpUtility.UrlPathEncode(path)));

                request.Accept = "*/*";
                request.AllowAutoRedirect = true;
                request.UserAgent = "http_requester/0.1";
                request.Timeout = 60000;
                request.Method = "DELETE";
                request.Headers.Add("Authorization", accessToken);

                request.ContentType = "application/json";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream(),
                                      ASCIIEncoding.UTF8);
                json = reader.ReadToEnd();
                reader.Close();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    resultado = json;
                    if (resultado.Contains("EventLog"))
                        throw new Exception(resultado);
                    if (trace && !onlyTraceErrors) { }

                       /* log.Info("\n*\n* [GET] Resultado al ejecutar el servicio web==>: \n* "
                            + base_URI + path + "\n* [RESULTADO] \n*" + resultado + "\n* [FIN RESULTADO] \n*\n*");*/
                }
                else
                {
                    throw new Exception(resultado);
                }
            }
            catch (WebException we)
            {

                if (trace)
                {
                   /* log.Error("\n* [GET] Error al ejecutar el servicio web() ==>: \n*" + base_URI + path);
                    log.Error("\n* [GET] Descripción del error ==>: " + we);
                    log.Error("\n* [GET] Método ejecutado ==>: " + System.Reflection.MethodBase.GetCurrentMethod().Name);*/
                }

                StreamReader reader = new StreamReader(we.Response.GetResponseStream(),
                ASCIIEncoding.UTF8);
                json = reader.ReadToEnd();
                reader.Close();

                if (!string.IsNullOrEmpty(json))
                {
                    try
                    {
                        EventLogEntry exception = new EventLogEntry();
                        exception = JsonConvert.DeserializeObject<EventLogEntry>(json);
                        //If exist an exception object, we will return as a string
                        if (exception != null)
                            //return exception.Description.ToString();
                            throw new Exception(exception.Description.ToString());
                        else
                        {
                            throw we;
                        }
                    }
                    catch (Exception ex)
                    {

                        throw new Exception(ex.ToString()); ;
                    }
                }
            }
            return resultado;
        }
    }
}
