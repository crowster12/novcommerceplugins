﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq; 

namespace UnitTestPakke
{
    [TestClass]
    public class UnitTestCouriers  //ServiceTest
    {
        private const string Base_URL = "https://seller.pakke.mx/api/v1/";
        private const string AccessToken = "WBUbUj33bsDloYkZzlAuM4s8gS0CKgriuqGS9rggiI0NnG0awwfB3AZtpNp9GJPC"; //Real tavo con saldo
        private const string AccessToken2 = "HaSfouuKwu8gpDxr63KdBIRINv9MjfnlrOlsnXYhWx5zuLF7wmbI6mkisADAwwvW";//Test Jesus sin saldo

        private const bool GenerateRef = false;

        [TestMethod]
        public void GetCouriers()
        {
            //CourierEntity courierInstance = PakkeLibrary.Services.CouriersService.Get(Base_URL, AccessToken, false);
            //Assert.AreNotEqual(courierInstance.ListCouriers.Count, 0); 
        }

        [TestMethod]
        public void GetPakkes()
        {
            ParcelEntity parcel = new ParcelEntity();
            parcel.Height = 1; //altura     
            parcel.Width = 2;//ancho
            parcel.Length = 1;//longitud
            parcel.Weight = 1;//peso
            Rate rate = new Rate();
            rate.parcel = parcel;     
            rate.ZipCodeFrom = "90127";
            rate.ZipCodeTo = "90135";

            PakkeEntity pakke = PakkeLibrary.Services.CouriersService.GetRates(Base_URL,rate, AccessToken, false);
            Assert.AreNotEqual(pakke.Pakke.Count, 0);

        }

        [TestMethod]
        public void GenerateReference()
        {

            //Set Address From Entity
           /* AddressFromEntity addressFromEntity = new AddressFromEntity();
            addressFromEntity.ZipCode = "90127";
            addressFromEntity.State = "MX-TLA";
            addressFromEntity.City = "TLAXCALA";
            addressFromEntity.Neighborhood = "Barrio Mexico";
            addressFromEntity.Address1 = "calle miraflores s/n";
            addressFromEntity.Address2 = "";
            addressFromEntity.Residential = false;

            //Set Adress to
            AddressToEntity addressToEntity = new AddressToEntity();
            addressToEntity.ZipCode = "90300";
            addressToEntity.State = "MX-TLA";
            addressToEntity.City = "APIZACO";
            addressToEntity.Neighborhood = "Colonia Centro";
            addressToEntity.Address1 = "Boulevard 16 de Septiembre s/n";
            addressToEntity.Address2 = "";
            addressToEntity.Residential = false;


            //Set Parcel
            ParcelEntity parcel = new ParcelEntity();
            parcel.Height = 10; //altura     
            parcel.Width = 10;//ancho
            parcel.Length = 20;//longitud
            parcel.Weight = 1;//peso


            //Set Sender
            SenderEntity senderEntity = new SenderEntity();
            senderEntity.Name = "Jesus Gonzalez";
            senderEntity.Phone1 = "2461714516";
            senderEntity.Phone2 = "";
            senderEntity.Email = "Jesus.grmtz@gmail.com";

            //Set Recipient 
            RecipientEntity recipientEntity = new RecipientEntity();
            recipientEntity.Name = "Maria Mendez";
            recipientEntity.Email = "yisus.tester@gmail.com";
            recipientEntity.CompanyName = "";
            recipientEntity.Phone1 = "2461714516";

            //Set the ShipmentREquest

            ShipmentRequest shipmentRequest = new ShipmentRequest();
            shipmentRequest.CourierCode = "FDX";
            shipmentRequest.CourierServiceId = "FEDEX_EXPRESS_SAVER";
            shipmentRequest.ResellerReference = "MNQA7WCO";//TCPIP-0817-23
            shipmentRequest.AddressFrom = addressFromEntity;
            shipmentRequest.AddressTo = addressToEntity;
            shipmentRequest.Parcel = parcel;

            shipmentRequest.Sender = senderEntity;
            shipmentRequest.Recipient = recipientEntity;

            //Set Content?
            shipmentRequest.Content = "Product";

            string access = string.Empty;
            if (GenerateRef)
            {
                access = AccessToken;
            }
            else {
                access = AccessToken2;
            }
            ShipmentResponse shipmentResponse = PakkeLibrary.Services.CouriersService.GenerateReference(Base_URL, shipmentRequest, access, true);
            Assert.AreNotEqual(shipmentResponse.ShipmentId,0);
            //Assert.AreNotEqual(shipmnetResponse.Pakke.Count, 0);*/
        }

        [TestMethod]
        public void DeserializePakke()
        {/*
            string jsonRespnse = "{\"Pakke\":[{\"CourierCode\":\"STF\",\"CourierName\":\"Estafeta\",\"CourierServiceId\":\"ESTAFETA_TERRESTRE_CONSUMO\",\"CourierServiceName\":\"Terrestre Consumo\",\"DeliveryDays\":\"2-5 días hab.\",\"CouponCode\":null,\"DiscountAmount\":0,\"TotalPrice\":127.18,\"EstimatedDeliveryDate\":\"2019-07-23\",\"BestOption\":true},{\"CourierCode\":\"FDX\",\"CourierName\":\"FedEx\",\"CourierServiceId\":\"FEDEX_EXPRESS_SAVER\",\"CourierServiceName\":\"Express Saver\",\"DeliveryDays\":\"3 días hab.\",\"CouponCode\":null,\"DiscountAmount\":0,\"TotalPrice\":185.24,\"EstimatedDeliveryDate\":\"2019-07-23\",\"BestOption\":false},{\"CourierCode\":\"FDX\",\"CourierName\":\"FedEx\",\"CourierServiceId\":\"FEDEX_STANDARD_OVERNIGHT\",\"CourierServiceName\":\"Standard Overnight\",\"DeliveryDays\":\"1 día hab.\",\"CouponCode\":null,\"DiscountAmount\":0,\"TotalPrice\":350.46,\"EstimatedDeliveryDate\":\"2019-07-23\",\"BestOption\":false},{\"CourierCode\":\"STF\",\"CourierName\":\"Estafeta\",\"CourierServiceId\":\"ESTAFETA_DIA_SIGUIENTE\",\"CourierServiceName\":\"Dia Siguiente\",\"DeliveryDays\":\"1 día hab.\",\"CouponCode\":null,\"DiscountAmount\":0,\"TotalPrice\":408.97,\"EstimatedDeliveryDate\":\"2019-07-23\",\"BestOption\":false}]}";
            PakkeEntity pakke = JsonConvert.DeserializeObject<PakkeEntity>(jsonRespnse);
            Assert.AreNotEqual(pakke.Pakke.Count, 0);*/
        }

        [TestMethod]
        public void DeserializePakkeResponseRe()
        {
            /*JObject o1 = JObject.Parse(File.ReadAllText(@"C:\Users\jesus.martinez\Desktop\sampleresponse.json"));
            using (StreamReader file = File.OpenText(@"C:\Users\jesus.martinez\Desktop\sampleresponse.json"))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JObject o2 = (JObject)JToken.ReadFrom(reader);
            }
            string jsonRes= o1.ToString();
            //string jsonRespnse = "{\"Pakke\":[{\"CourierCode\":\"STF\",\"CourierName\":\"Estafeta\",\"CourierServiceId\":\"ESTAFETA_TERRESTRE_CONSUMO\",\"CourierServiceName\":\"Terrestre Consumo\",\"DeliveryDays\":\"2-5 días hab.\",\"CouponCode\":null,\"DiscountAmount\":0,\"TotalPrice\":127.18,\"EstimatedDeliveryDate\":\"2019-07-23\",\"BestOption\":true},{\"CourierCode\":\"FDX\",\"CourierName\":\"FedEx\",\"CourierServiceId\":\"FEDEX_EXPRESS_SAVER\",\"CourierServiceName\":\"Express Saver\",\"DeliveryDays\":\"3 días hab.\",\"CouponCode\":null,\"DiscountAmount\":0,\"TotalPrice\":185.24,\"EstimatedDeliveryDate\":\"2019-07-23\",\"BestOption\":false},{\"CourierCode\":\"FDX\",\"CourierName\":\"FedEx\",\"CourierServiceId\":\"FEDEX_STANDARD_OVERNIGHT\",\"CourierServiceName\":\"Standard Overnight\",\"DeliveryDays\":\"1 día hab.\",\"CouponCode\":null,\"DiscountAmount\":0,\"TotalPrice\":350.46,\"EstimatedDeliveryDate\":\"2019-07-23\",\"BestOption\":false},{\"CourierCode\":\"STF\",\"CourierName\":\"Estafeta\",\"CourierServiceId\":\"ESTAFETA_DIA_SIGUIENTE\",\"CourierServiceName\":\"Dia Siguiente\",\"DeliveryDays\":\"1 día hab.\",\"CouponCode\":null,\"DiscountAmount\":0,\"TotalPrice\":408.97,\"EstimatedDeliveryDate\":\"2019-07-23\",\"BestOption\":false}]}";
            ShipmentResponse response = JsonConvert.DeserializeObject<ShipmentResponse>(jsonRes);
            Assert.AreNotEqual(response.ResellerId, 1);*/
        }

 

    }
}
