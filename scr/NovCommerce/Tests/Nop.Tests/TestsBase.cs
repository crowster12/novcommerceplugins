﻿using System;
using System.Security.Principal;
using Moq;
using NUnit.Framework;

namespace Nop.Tests
{
    public abstract class TestsBase
    {
        protected MockRepository mocks;

        [SetUp]
        public virtual void SetUp()
        {
            mocks = new MockRepository(MockBehavior.Loose);
        }

        [TearDown]
        public virtual void TearDown()
        {
            mocks?.VerifyAll();
        }

        protected static IPrincipal CreatePrincipal(string name, params string[] roles)
        {
            //Create Download file to add the attachmentID
            Nop.Core.Domain.Media.Download download = null;
            download = new Core.Domain.Media.Download();
            Guid obj = Guid.NewGuid();
            download.UseDownloadUrl = true;
            //download.DownloadUrl = dashboardPath;
            // download.DownloadBinary = bytes;
            download.Filename = "Guia de referencia ";
            download.Extension = ".pdf";
            download.IsNew = true;
            download.DownloadGuid = obj;
           // _downloadService.InsertDownload(download);
           // Nop.Core.Domain.Media.Download download2 = _downloadService.GetDownloadByGuid(obj);

            return new GenericPrincipal(new GenericIdentity(name, "TestIdentity"), roles);
        }
    }
}
